namespace semesterprojekt_1
{
    partial class frm_level1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbx_eingabe = new System.Windows.Forms.TextBox();
            this.lbl_lvl1_count = new System.Windows.Forms.Label();
            this.tmr_lvl1 = new System.Windows.Forms.Timer(this.components);
            this.lbl_lvl1_zeit = new System.Windows.Forms.Label();
            this.p27 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.p26 = new System.Windows.Forms.PictureBox();
            this.p25 = new System.Windows.Forms.PictureBox();
            this.p19 = new System.Windows.Forms.PictureBox();
            this.p22 = new System.Windows.Forms.PictureBox();
            this.p23 = new System.Windows.Forms.PictureBox();
            this.p21 = new System.Windows.Forms.PictureBox();
            this.p20 = new System.Windows.Forms.PictureBox();
            this.p24 = new System.Windows.Forms.PictureBox();
            this.p18 = new System.Windows.Forms.PictureBox();
            this.p17 = new System.Windows.Forms.PictureBox();
            this.p16 = new System.Windows.Forms.PictureBox();
            this.p15 = new System.Windows.Forms.PictureBox();
            this.p14 = new System.Windows.Forms.PictureBox();
            this.p13 = new System.Windows.Forms.PictureBox();
            this.p12 = new System.Windows.Forms.PictureBox();
            this.p11 = new System.Windows.Forms.PictureBox();
            this.p7 = new System.Windows.Forms.PictureBox();
            this.p10 = new System.Windows.Forms.PictureBox();
            this.p9 = new System.Windows.Forms.PictureBox();
            this.p8 = new System.Windows.Forms.PictureBox();
            this.p6 = new System.Windows.Forms.PictureBox();
            this.p5 = new System.Windows.Forms.PictureBox();
            this.p4 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.PictureBox();
            this.p3 = new System.Windows.Forms.PictureBox();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.btn_lvl1_aufgeben = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.p27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // tbx_eingabe
            // 
            this.tbx_eingabe.BackColor = System.Drawing.Color.OldLace;
            this.tbx_eingabe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbx_eingabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx_eingabe.Location = new System.Drawing.Point(29, 167);
            this.tbx_eingabe.Name = "tbx_eingabe";
            this.tbx_eingabe.Size = new System.Drawing.Size(200, 35);
            this.tbx_eingabe.TabIndex = 1;
            this.tbx_eingabe.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lbl_lvl1_count
            // 
            this.lbl_lvl1_count.AutoSize = true;
            this.lbl_lvl1_count.BackColor = System.Drawing.Color.Transparent;
            this.lbl_lvl1_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lvl1_count.Location = new System.Drawing.Point(27, 205);
            this.lbl_lvl1_count.Name = "lbl_lvl1_count";
            this.lbl_lvl1_count.Size = new System.Drawing.Size(49, 24);
            this.lbl_lvl1_count.TabIndex = 4;
            this.lbl_lvl1_count.Text = "0/27";
            // 
            // tmr_lvl1
            // 
            this.tmr_lvl1.Interval = 1000;
            this.tmr_lvl1.Tick += new System.EventHandler(this.tmr_lvl1_Tick);
            // 
            // lbl_lvl1_zeit
            // 
            this.lbl_lvl1_zeit.AutoSize = true;
            this.lbl_lvl1_zeit.BackColor = System.Drawing.Color.Transparent;
            this.lbl_lvl1_zeit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lvl1_zeit.Location = new System.Drawing.Point(26, 80);
            this.lbl_lvl1_zeit.Name = "lbl_lvl1_zeit";
            this.lbl_lvl1_zeit.Size = new System.Drawing.Size(64, 29);
            this.lbl_lvl1_zeit.TabIndex = 5;
            this.lbl_lvl1_zeit.Text = "Zeit:";
            // 
            // p27
            // 
            this.p27.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p27.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p27.Location = new System.Drawing.Point(742, 446);
            this.p27.Name = "p27";
            this.p27.Size = new System.Drawing.Size(5, 5);
            this.p27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p27.TabIndex = 32;
            this.p27.TabStop = false;
            this.p27.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlText;
            this.pictureBox2.BackgroundImage = global::semesterprojekt_1.Properties.Resources.eu_staaten;
            this.pictureBox2.Image = global::semesterprojekt_1.Properties.Resources.zypern;
            this.pictureBox2.Location = new System.Drawing.Point(725, 441);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 17);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // p26
            // 
            this.p26.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p26.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p26.Location = new System.Drawing.Point(625, 382);
            this.p26.Name = "p26";
            this.p26.Size = new System.Drawing.Size(13, 10);
            this.p26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p26.TabIndex = 30;
            this.p26.TabStop = false;
            this.p26.Visible = false;
            // 
            // p25
            // 
            this.p25.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p25.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p25.Location = new System.Drawing.Point(594, 360);
            this.p25.Name = "p25";
            this.p25.Size = new System.Drawing.Size(13, 10);
            this.p25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p25.TabIndex = 29;
            this.p25.TabStop = false;
            this.p25.Visible = false;
            // 
            // p19
            // 
            this.p19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p19.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p19.Location = new System.Drawing.Point(438, 441);
            this.p19.Name = "p19";
            this.p19.Size = new System.Drawing.Size(13, 10);
            this.p19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p19.TabIndex = 28;
            this.p19.TabStop = false;
            this.p19.Visible = false;
            // 
            // p22
            // 
            this.p22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p22.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p22.Location = new System.Drawing.Point(625, 366);
            this.p22.Name = "p22";
            this.p22.Size = new System.Drawing.Size(10, 10);
            this.p22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p22.TabIndex = 27;
            this.p22.TabStop = false;
            this.p22.Visible = false;
            // 
            // p23
            // 
            this.p23.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p23.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p23.Location = new System.Drawing.Point(594, 398);
            this.p23.Name = "p23";
            this.p23.Size = new System.Drawing.Size(5, 5);
            this.p23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p23.TabIndex = 27;
            this.p23.TabStop = false;
            this.p23.Visible = false;
            // 
            // p21
            // 
            this.p21.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p21.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p21.Location = new System.Drawing.Point(605, 167);
            this.p21.Name = "p21";
            this.p21.Size = new System.Drawing.Size(20, 15);
            this.p21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p21.TabIndex = 26;
            this.p21.TabStop = false;
            this.p21.Visible = false;
            // 
            // p20
            // 
            this.p20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p20.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p20.Location = new System.Drawing.Point(660, 388);
            this.p20.Name = "p20";
            this.p20.Size = new System.Drawing.Size(13, 20);
            this.p20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p20.TabIndex = 25;
            this.p20.TabStop = false;
            this.p20.Visible = false;
            // 
            // p24
            // 
            this.p24.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p24.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p24.Location = new System.Drawing.Point(466, 437);
            this.p24.Name = "p24";
            this.p24.Size = new System.Drawing.Size(19, 14);
            this.p24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p24.TabIndex = 24;
            this.p24.TabStop = false;
            this.p24.Visible = false;
            // 
            // p18
            // 
            this.p18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p18.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p18.Location = new System.Drawing.Point(625, 320);
            this.p18.Name = "p18";
            this.p18.Size = new System.Drawing.Size(24, 15);
            this.p18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p18.TabIndex = 23;
            this.p18.TabStop = false;
            this.p18.Visible = false;
            // 
            // p17
            // 
            this.p17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p17.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p17.Location = new System.Drawing.Point(593, 376);
            this.p17.Name = "p17";
            this.p17.Size = new System.Drawing.Size(13, 10);
            this.p17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p17.TabIndex = 22;
            this.p17.TabStop = false;
            this.p17.Visible = false;
            // 
            // p16
            // 
            this.p16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p16.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p16.Location = new System.Drawing.Point(532, 333);
            this.p16.Name = "p16";
            this.p16.Size = new System.Drawing.Size(13, 10);
            this.p16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p16.TabIndex = 21;
            this.p16.TabStop = false;
            this.p16.Visible = false;
            // 
            // p15
            // 
            this.p15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p15.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p15.Location = new System.Drawing.Point(616, 486);
            this.p15.Name = "p15";
            this.p15.Size = new System.Drawing.Size(5, 5);
            this.p15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p15.TabIndex = 20;
            this.p15.TabStop = false;
            this.p15.Visible = false;
            // 
            // p14
            // 
            this.p14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p14.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p14.Location = new System.Drawing.Point(532, 360);
            this.p14.Name = "p14";
            this.p14.Size = new System.Drawing.Size(5, 5);
            this.p14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p14.TabIndex = 19;
            this.p14.TabStop = false;
            this.p14.Visible = false;
            // 
            // p13
            // 
            this.p13.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p13.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p13.Location = new System.Drawing.Point(660, 297);
            this.p13.Name = "p13";
            this.p13.Size = new System.Drawing.Size(13, 10);
            this.p13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p13.TabIndex = 18;
            this.p13.TabStop = false;
            this.p13.Visible = false;
            // 
            // p12
            // 
            this.p12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p12.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p12.Location = new System.Drawing.Point(669, 282);
            this.p12.Name = "p12";
            this.p12.Size = new System.Drawing.Size(13, 10);
            this.p12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p12.TabIndex = 17;
            this.p12.TabStop = false;
            this.p12.Visible = false;
            // 
            // p11
            // 
            this.p11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p11.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p11.Location = new System.Drawing.Point(605, 398);
            this.p11.Name = "p11";
            this.p11.Size = new System.Drawing.Size(10, 10);
            this.p11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p11.TabIndex = 16;
            this.p11.TabStop = false;
            this.p11.Visible = false;
            // 
            // p7
            // 
            this.p7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p7.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p7.Location = new System.Drawing.Point(505, 376);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(31, 22);
            this.p7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p7.TabIndex = 15;
            this.p7.TabStop = false;
            this.p7.Visible = false;
            // 
            // p10
            // 
            this.p10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p10.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p10.Location = new System.Drawing.Point(567, 407);
            this.p10.Name = "p10";
            this.p10.Size = new System.Drawing.Size(13, 10);
            this.p10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p10.TabIndex = 14;
            this.p10.TabStop = false;
            this.p10.Visible = false;
            // 
            // p9
            // 
            this.p9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p9.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p9.Location = new System.Drawing.Point(438, 320);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(13, 10);
            this.p9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p9.TabIndex = 13;
            this.p9.TabStop = false;
            this.p9.Visible = false;
            // 
            // p8
            // 
            this.p8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p8.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p8.Location = new System.Drawing.Point(646, 462);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(13, 10);
            this.p8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p8.TabIndex = 12;
            this.p8.TabStop = false;
            this.p8.Visible = false;
            // 
            // p6
            // 
            this.p6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p6.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p6.Location = new System.Drawing.Point(660, 192);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(22, 19);
            this.p6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p6.TabIndex = 11;
            this.p6.TabStop = false;
            this.p6.Visible = false;
            // 
            // p5
            // 
            this.p5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p5.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p5.Location = new System.Drawing.Point(669, 257);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(13, 10);
            this.p5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p5.TabIndex = 10;
            this.p5.TabStop = false;
            this.p5.Visible = false;
            // 
            // p4
            // 
            this.p4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p4.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p4.Location = new System.Drawing.Point(559, 297);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(13, 10);
            this.p4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p4.TabIndex = 9;
            this.p4.TabStop = false;
            this.p4.Visible = false;
            // 
            // p2
            // 
            this.p2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p2.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p2.Location = new System.Drawing.Point(523, 350);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(13, 10);
            this.p2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p2.TabIndex = 8;
            this.p2.TabStop = false;
            this.p2.Visible = false;
            // 
            // p3
            // 
            this.p3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p3.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p3.Location = new System.Drawing.Point(660, 426);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(13, 10);
            this.p3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p3.TabIndex = 8;
            this.p3.TabStop = false;
            this.p3.Visible = false;
            // 
            // p1
            // 
            this.p1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.p1.Image = global::semesterprojekt_1.Properties.Resources.punkt;
            this.p1.Location = new System.Drawing.Point(559, 333);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(21, 18);
            this.p1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p1.TabIndex = 7;
            this.p1.TabStop = false;
            this.p1.Visible = false;
            // 
            // btn_lvl1_aufgeben
            // 
            this.btn_lvl1_aufgeben.BackgroundImage = global::semesterprojekt_1.Properties.Resources.button_hintergrund;
            this.btn_lvl1_aufgeben.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_lvl1_aufgeben.FlatAppearance.BorderSize = 3;
            this.btn_lvl1_aufgeben.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btn_lvl1_aufgeben.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_lvl1_aufgeben.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_lvl1_aufgeben.ForeColor = System.Drawing.Color.White;
            this.btn_lvl1_aufgeben.Location = new System.Drawing.Point(29, 462);
            this.btn_lvl1_aufgeben.Name = "btn_lvl1_aufgeben";
            this.btn_lvl1_aufgeben.Size = new System.Drawing.Size(158, 53);
            this.btn_lvl1_aufgeben.TabIndex = 1;
            this.btn_lvl1_aufgeben.Text = "Aufgeben?";
            this.btn_lvl1_aufgeben.UseVisualStyleBackColor = true;
            this.btn_lvl1_aufgeben.Click += new System.EventHandler(this.btn_lvl1_aufgeben_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::semesterprojekt_1.Properties.Resources.eu_staaten;
            this.pictureBox1.Location = new System.Drawing.Point(205, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(693, 502);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::semesterprojekt_1.Properties.Resources.frog5;
            this.pictureBox3.Location = new System.Drawing.Point(44, 366);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(80, 42);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 33;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::semesterprojekt_1.Properties.Resources.pyramide;
            this.pictureBox4.Location = new System.Drawing.Point(-54, 333);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(368, 184);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 34;
            this.pictureBox4.TabStop = false;
            // 
            // frm_level1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.ClientSize = new System.Drawing.Size(843, 546);
            this.Controls.Add(this.p27);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.p26);
            this.Controls.Add(this.p25);
            this.Controls.Add(this.p19);
            this.Controls.Add(this.p22);
            this.Controls.Add(this.p23);
            this.Controls.Add(this.p21);
            this.Controls.Add(this.p20);
            this.Controls.Add(this.p24);
            this.Controls.Add(this.p18);
            this.Controls.Add(this.p17);
            this.Controls.Add(this.p16);
            this.Controls.Add(this.p15);
            this.Controls.Add(this.p14);
            this.Controls.Add(this.p13);
            this.Controls.Add(this.p12);
            this.Controls.Add(this.p11);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.p10);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.lbl_lvl1_zeit);
            this.Controls.Add(this.lbl_lvl1_count);
            this.Controls.Add(this.tbx_eingabe);
            this.Controls.Add(this.btn_lvl1_aufgeben);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Name = "frm_level1";
            this.Text = "Level 1";
            this.Load += new System.EventHandler(this.frm_level1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.p27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_lvl1_aufgeben;
        private System.Windows.Forms.TextBox tbx_eingabe;
        private System.Windows.Forms.Label lbl_lvl1_count;
        private System.Windows.Forms.Timer tmr_lvl1;
        private System.Windows.Forms.Label lbl_lvl1_zeit;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.PictureBox p3;
        private System.Windows.Forms.PictureBox p2;
        private System.Windows.Forms.PictureBox p4;
        private System.Windows.Forms.PictureBox p5;
        private System.Windows.Forms.PictureBox p6;
        private System.Windows.Forms.PictureBox p8;
        private System.Windows.Forms.PictureBox p9;
        private System.Windows.Forms.PictureBox p10;
        private System.Windows.Forms.PictureBox p7;
        private System.Windows.Forms.PictureBox p11;
        private System.Windows.Forms.PictureBox p12;
        private System.Windows.Forms.PictureBox p13;
        private System.Windows.Forms.PictureBox p14;
        private System.Windows.Forms.PictureBox p15;
        private System.Windows.Forms.PictureBox p16;
        private System.Windows.Forms.PictureBox p17;
        private System.Windows.Forms.PictureBox p18;
        private System.Windows.Forms.PictureBox p24;
        private System.Windows.Forms.PictureBox p20;
        private System.Windows.Forms.PictureBox p21;
        private System.Windows.Forms.PictureBox p23;
        private System.Windows.Forms.PictureBox p22;
        private System.Windows.Forms.PictureBox p19;
        private System.Windows.Forms.PictureBox p25;
        private System.Windows.Forms.PictureBox p26;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox p27;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}