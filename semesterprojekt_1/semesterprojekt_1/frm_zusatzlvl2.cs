﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_zusatzlvl2 : Form
    {
        int a = 0;
        int richtig;
        int frage;
        int level = 8;
        string ben;
        int id;
        bool lvlz2;
        int leben;
        public frm_zusatzlvl2()
        {
            InitializeComponent();
            
        }
        public frm_zusatzlvl2(string benutzer, int idd, int leben)
        {
            InitializeComponent();
            ben = benutzer;
            id = idd;
            this.leben = leben;
        }
        // Welches dieser Länder ist kein Binnenland? Welches Land ist keine Monarchie? Wie viele Zeitzonen hat China? Das größte Land Afrikas heißt...
        private void frm_zusatzlvl2_Load(object sender, EventArgs e)
        {
            Spiel();
            frage = 0;
            lbl_hs.Text = "Highscore: " + richtig;
        }
        private void Spiel()
        {
            if(frage == 0)
            {
                lbl_fragee.Text = "Frage 1 von 5";
                lbl_frage.Text = "Welches dieser Länder ist kein Binnenland?";
                btn_1.Text = "Aserbaidschan";
                btn_2.Text = "Georgien";
                btn_3.Text = "Armenien";
                if (a == 1)
                {
                   
                    richtig = richtig - 20;
                    lbl_falsch1.Visible = true;
                }
                else if (a == 2)
                {
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;
                }
            }
            if (frage == 1)
            {
                lbl_fragee.Text = "Frage 2 von 5";
                lbl_falsch1.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_frage.Text = "Welches Land ist keine Monarchie?";
                btn_1.Text = "Kirgisistan";
                btn_2.Text = "Schweden";
                btn_3.Text = "Vereinigte Arabische Emirate";
                if (a == 1)
                {
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                    
                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;
                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;
                }
            }
            if (frage == 2)
            {
                lbl_fragee.Text = "Frage 3 von 5";
                lbl_falsch2.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_frage.Text = "Wie viele Zeitzonen hat China?";
                btn_1.Text = "Zwölf";
                btn_2.Text = "Eine";
                btn_3.Text = "Sieben";
                if (a == 1)
                {
                    richtig = richtig - 20;
                    lbl_falsch1.Visible = true;

                }
                else if (a == 2)
                {
                    
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;
                }
            }
            if (frage == 3)
            {
                lbl_fragee.Text = "Frage 4 von 5";
                lbl_falsch1.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_frage.Text = "Das größte Land Afrikas heißt...";
                btn_1.Text = "Südafrika";
                btn_2.Text = "Sudan";
                btn_3.Text = "Algerien";
                if (a == 1)
                {
                    richtig = richtig - 20;
                    lbl_falsch1.Visible = true;

                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;

                }
                else if (a == 3)
                {
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                }
            }
            if (frage == 4)
            {
                lbl_fragee.Text = "Frage 5 von 5";
                lbl_falsch1.Visible = false;
                lbl_falsch2.Visible = false;
                lbl_frage.Text = "Welches dieser Länder liegt am Mittelmeer?";
                btn_1.Text = "San Marino";
                btn_2.Text = "Jordanien";
                btn_3.Text = "Slowenien";
                if (a == 1)
                {
                    richtig = richtig - 20;
                    lbl_falsch1.Visible = true;

                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;

                }
                else if (a == 3)
                {
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                }
            }
            if (frage == 5)
            {
                if (richtig ==150)
                {
                    MessageBox.Show("Du hast alle Fragen richtig beantwortet, also bekommst du ein Herz. Dein Highscore für dieses Zusatzlevel liegt bei " + richtig + " Punkten.", "Zusatzlevel 2");
                    lvlz2 = true;
                    cls_dataprovider.Highscore(ben, richtig, level, id);
                    leben--;
                    cls_spiel.Main = lvlz2;
                    cls_spiel.Highscore = richtig;
                    cls_spiel.Level = level;
                    cls_spiel.Leben = leben;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Du hast alle Fragen beantwortet. Leider nicht alle richtig, du bekommst also kein Herz. Dein Highscore für dieses Zusatzlevel liegt bei " + richtig + " Punkten.", "Zusatzlevel 2");
                    lvlz2 = true;
                    cls_dataprovider.Highscore(ben, richtig, level, id);
                    cls_spiel.Main = lvlz2;
                    cls_spiel.Highscore = richtig;
                    cls_spiel.Level = level;
                    cls_spiel.Leben = leben;
                    this.Close();
                }

            }
            lbl_hs.Text = "Highscore: " + richtig;
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            a = 2;
            Spiel();

        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            a = 1;
            Spiel();
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            a = 3;
            Spiel();
        }

    }
}
