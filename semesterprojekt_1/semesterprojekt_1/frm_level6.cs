﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_level6 : Form
    {
        bool lvl6;
        int high=0;
        int level = 6;
        int count;
        int leben;
        string ben;
        int id;
        string click;
        int land;
        int zeit;
        List<string> lander = new List<string> { "Northern Lowlands", "Southern Highlands", "Abflusskanäle", "Alba Mons", "Amazonis Plantitia", "Argyre Planitia", "Arabia Terra", "Chryse Planitia", "Elysium Mons", "Hellas Planitia", "Isidis Planitia", "Noachis Terra", "Olympus Mons", "Tempe Terra", "Tharsis", "Tharsis Montes", "Utopia Planitia", "Valles Marineris","Vastitas Borealis" };
        //List<string> lander = new List<string> { "Northern Lowlands", "Southern Highlands", "Abflusskanäle" };
        Random r = new Random();
        public frm_level6()
        {
            InitializeComponent();
        }
        public frm_level6(string benutzer, int lb, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lb;
            id = idd;
        }

        private void frm_level6_Load(object sender, EventArgs e)
        {
            land = r.Next(lander.Count);
            lbl_gebiete.Text = "Klicke auf " + lander[land];
            zeit = 300;
            tmr_lvl6.Start();
        }
        private void Spiel()
        {     
          if (lander[land] == click)
          {
              high++;
              count++;
              lbl_high.Text = "Highscore: " + high;
              lander.Remove(lander[land]);
                if (lander.Count > 0)
                {
                    land = r.Next(lander.Count);
                    lbl_gebiete.Text = "Klicke auf "+lander[land];
                }
                if (count == 19)
                    {
                        lvl6 = true;
                    tmr_lvl6.Stop();
                    high = zeit + high;
                        MessageBox.Show("Du hast es geschafft!" +
                            "Dein Highscore beträgt " + high);
                        cls_dataprovider.Highscore(ben, high, level, id);
                        cls_spiel.Main = lvl6;
                        cls_spiel.Highscore = high;
                        cls_spiel.Leben = leben;
                        cls_spiel.Level = level;
                    cls_spiel.Highscorelvl6= high;
                        this.Close();
                    }
          }
                else if (lander[land] != click)
                {
                    high--;
                    lbl_high.Text = "Highscore: " + high;
                }

        }

        private void pbx_north_Click(object sender, EventArgs e)
        {
            click = "Northern Lowlands";
            Spiel();
        }

        private void pbx_south_Click(object sender, EventArgs e)
        {
            click = "Southern Highlands";
            Spiel();
        }

        private void pbx_abfluss_Click(object sender, EventArgs e)
        {
            click = "Abflusskanäle";
            Spiel();
        }

        private void pbx_amazonis_Click(object sender, EventArgs e)
        {
            click = "Amazonis Plantitia";
            Spiel();
        }

        private void pbx_olympus_Click(object sender, EventArgs e)
        {
            click = "Olympus Mons";
            Spiel();
        }

        private void pbx_alba_Click(object sender, EventArgs e)
        {
            click = "Alba Mons";
            Spiel();
        }

        private void pbx_tempe_Click(object sender, EventArgs e)
        {
            click = "Tempe Terra";
            Spiel();
        }

        private void pbx_tharsism_Click(object sender, EventArgs e)
        {
            click = "Tharsis Montes";
            Spiel();
        }

        private void pbx_tharsisi_Click(object sender, EventArgs e)
        {
            click = "Tharsis";
            Spiel();
        }

        private void pbx_valles_Click(object sender, EventArgs e)
        {
            click = "Valles Marineris";
            Spiel();
        }

        private void pbx_chryse_Click(object sender, EventArgs e)
        {
            click = "Chryse Planitia";
            Spiel();
        }

        private void pbx_argyre_Click(object sender, EventArgs e)
        {
            click = "Argyre Planitia";
            Spiel();
        }

        private void pbx_arabia_Click(object sender, EventArgs e)
        {
            click = "Arabia Terra";
            Spiel();
        }

        private void pbx_noachis_Click(object sender, EventArgs e)
        {
            click = "Noachis Terra";
            Spiel();
        }

        private void pbx_vasitas_Click(object sender, EventArgs e)
        {
            click = "Vastitas Borealis";
            Spiel();
        }

        private void pbx_utopia_Click(object sender, EventArgs e)
        {
            click = "Utopia Planitia";
            Spiel();
        }

        private void pbx_elysium_Click(object sender, EventArgs e)
        {
            click = "Elysium Mons";
            Spiel();
        }

        private void pbx_isidis_Click(object sender, EventArgs e)
        {
            click = "Isidis Planitia";
            Spiel();
        }

        private void pbx_hellas_Click(object sender, EventArgs e)
        {
            click = "Hellas Planitia";
            Spiel();
        }

        private void btn_aufgeben_Click(object sender, EventArgs e)
        {
            lvl6 = true;
            tmr_lvl6.Stop();
            MessageBox.Show("Das ist sehr schade.", "Gib nicht auf!");
            leben++;
            cls_spiel.Leben = leben;
            cls_spiel.Highscore = high;
            cls_spiel.Level = level;
            cls_spiel.Main = lvl6;
            this.Close();
        }

        private void tmr_lvl6_Tick(object sender, EventArgs e)
        {
            lbl_zeit.Text = "Zeit:" + zeit--.ToString();

            if (zeit < 1)
            {
                lvl6 = true;
                tmr_lvl6.Stop();
                MessageBox.Show("Die Zeit ist aus.");
                leben++;
                cls_spiel.Leben = leben;
                cls_spiel.Highscore = high;
                cls_spiel.Level = level;
                cls_spiel.Main = lvl6;
                this.Close();
            }
        }
    }
}
