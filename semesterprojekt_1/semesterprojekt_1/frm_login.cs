using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_main : Form
    {
        int leben1;
        int high;
        int id;
        string ben;
        string passwort;
        bool main = false;
        string alle;
        int lvl;
        int level;
        cls_konto k;
        string name;
        int high1;
        int high2;
        int high3;
        int high4;
        int high5;
        int high6;
        List<cls_konto> kontos = new List<cls_konto>();
        public frm_main()
        {
            InitializeComponent();
            lbx_board.DisplayMember = "Ausgabe";
        }
       

        private void btn_loschen_Click(object sender, EventArgs e)
        {
            tbx_benutzer.Text = "";
            tbx_passwort.Text = "";
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            pbx_lvl1.Visible = true;
            ben = tbx_benutzer.Text;
            passwort = tbx_passwort.Text;
            id = cls_dataprovider.GetID(ben, passwort);
            if (id>0)
            {
                lbl_benutzer.Visible = false;
                lbl_passwort.Visible = false;
                tbx_benutzer.Visible = false;
                tbx_passwort.Visible = false;
                btn_login.Visible = false;
                btn_loschen.Visible = false;
                btn_signup.Visible = false;
                lbl_name.Text = tbx_benutzer.Text;

                lbl_name.Visible = true;
                pbx_avatar.Visible = true;
                btn_ausloggen.Visible = true;
                btn_account.Visible = true; 
                btn_daten.Visible = true;
                rbtn_alle.Visible = true;
                rbtn_ich.Visible = true;
                id = cls_dataprovider.GetID(ben, passwort);
            }
            else
            {
                MessageBox.Show("Geben Sie einen gültigen Benutzer oder Passwort ein!");
            }
        }

        private void btn_signup_Click(object sender, EventArgs e)
        {
            tbx_benutzer.Text = "";
            tbx_passwort.Text = "";
            frm_signup su = new frm_signup();
            su.ShowDialog();
        }

        private void frm_login_Load(object sender, EventArgs e)
        {
            Leben();
            main = cls_spiel.Main;
            leben1 = cls_spiel.Leben;
            level = cls_spiel.Level;
            high = cls_spiel.Highscore;
            Aktualisieren();
            if (main ==true)
            {
                Aktualisieren();
                lbl_benutzer.Visible = false;
                lbl_passwort.Visible = false;
                tbx_benutzer.Visible = false;
                tbx_passwort.Visible = false;
                btn_login.Visible = false;
                btn_loschen.Visible = false;
                btn_signup.Visible = false;

                lbl_name.Visible = true;
                pbx_avatar.Visible = true;
                btn_ausloggen.Visible = true;
                btn_account.Visible = true;
                btn_daten.Visible = true;
                rbtn_alle.Visible = true;
                rbtn_ich.Visible = true;
            }
        }
        private void Leben()
        {
            if (leben1 == 0)
            {
                pbx_grau1.Visible = false;
                pbx_grau2.Visible = false;
                pbx_grau3.Visible = false;
            }
            else if (leben1 == 1)
            {
                pbx_grau1.Visible = true;
                pbx_grau2.Visible = false;
                pbx_grau3.Visible = false;
            }
            else if (leben1 == 2)
            {
                pbx_grau1.Visible = true;
                pbx_grau2.Visible = true;
                pbx_grau3.Visible = false;
            }
            else if (leben1 == 3)
            {
                MessageBox.Show("Du hast keine Leben mehr. Das Spiel ist vorbei");
                this.Close();
            }
        }
        private void Aktualisieren()
        {
            kontos.Clear();
            lbx_board.Items.Clear();
            cls_dataprovider.LoadData(kontos, lvl, alle, ben);
            foreach (cls_konto k in kontos)
            {
                lbx_board.Items.Add(k);
            }
        }
        private void btn_ausloggen_Click(object sender, EventArgs e)
        {
            Aus();
        }
        private void cbx_level_TextChanged(object sender, EventArgs e)
        {
            if (cbx_level.Text == "Alle")
            {
                lvl = 0;
            }
            else if (cbx_level.Text == "1")
            {
                lvl = 1;
            }
            else if (cbx_level.Text == "2")
            {
                lvl = 2;
            }
            else if (cbx_level.Text == "3")
            {
                lvl = 3;
            }
            else if (cbx_level.Text == "4")
            {
                lvl = 4;
            }
            else if (cbx_level.Text == "5")
            {
                lvl = 5;
            }
            else if (cbx_level.Text == "6")
            {
                lvl = 6;
            }
            else if(cbx_level.Text=="7")
            {
                lvl = 7;
            }
            else if(cbx_level.Text=="8")
            {
                lvl = 8;
            }
            Aktualisieren();
        }

        private void Aus()
        {
            lbl_name.Visible = false;
            pbx_avatar.Visible = false;
            btn_ausloggen.Visible = false;
            btn_account.Visible = false;
            btn_daten.Visible = false;
            rbtn_alle.Visible = false;
            rbtn_ich.Visible = false;
            rbtn_ich.Checked = false;

            lbl_benutzer.Visible = true;
            lbl_passwort.Visible = true;
            tbx_benutzer.Visible = true;
            tbx_passwort.Visible = true;
            btn_login.Visible = true;
            btn_loschen.Visible = true;
            btn_signup.Visible = true;
            tbx_benutzer.Text = "";
            tbx_passwort.Text = "";

            pbx_lvl1.Visible = false;
            pbx_lvl2.Visible=false;
            pbx_lvl3.Visible=false;
            pbx_lvl4.Visible=false;
            pbx_lvl5.Visible=false;
            pbx_lvl6.Visible=false;
            pbx_zlvl1.Visible=false;
            pbx_zlvl2.Visible=false;
            pbx_1.Visible=false;
            pbx_2.Visible=false;
            pbx_3.Visible=false;
            pbx_4.Visible=false;
            pbx_5.Visible=false;
            pbx_6.Visible=false;
            pbx_7.Visible=false;
        }
        private void btn_account_Click(object sender, EventArgs e)
        {
            cls_dataprovider.DeleteHighscore(id);
            cls_dataprovider.DeleteSignup(ben, passwort);

            Aus();
            Aktualisieren();
        }

        private void btn_daten_Click(object sender, EventArgs e)
        {
            cls_dataprovider.DeleteHighscore(id);
            Aktualisieren();
        }

        private void rbtn_alle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_alle.Checked)
            {
                alle = "alle";
            }
            Aktualisieren() ;
        }

        private void rbtn_ich_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_ich.Checked)
            {
                alle = "ich";
            }
            Aktualisieren() ;
        }
        private void pbx_lvl1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Finde die 27 EU-Länder. Achte auf die Zeit: Du hast nur 3 Minuten.", "Level 1 Bemerkung");
            frm_level1 l1 = new frm_level1(ben, leben1, id);
            l1.ShowDialog();
            Aktualisieren();
            leben1 = cls_spiel.Leben;
            Leben();
            high1 = cls_spiel.Highscorelvl1;
            if (high1 > 100)
            {
                pbx_lvl2.Visible = true;
                pbx_1.Visible = true;
            }
            else if (high1 < 100)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 100 haben, um fortzufahren!");
            }
        }

        private void pbx_lvl2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ordne die Länder den passenden Flaggen zu. Du hast 2 Minute Zeit.");
            frm_level2 lvl2 = new frm_level2(ben, leben1, id);
            lvl2.ShowDialog();
            Aktualisieren();
            high2 = cls_spiel.Highscorelvl2;
            leben1 = cls_spiel.Leben;
            if (high2 > 50)
            {
                pbx_lvl3.Visible = true;
                pbx_3.Visible = true;
                pbx_zlvl1.Visible = true;
                pbx_2.Visible = true;
            }
            else if (high2 < 50)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 50 haben, um fortzufahren!");
            }
            Leben();
        }

        private void pbx_zlvl1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Das hier ist ein Zusatzlevel mit mittelschweren Fragen. Beantwortest du sie richtig, bekommst du ein Herz. Wenn du falsch antwortest kannst du aber kein Herz verlieren!");
            frm_zusatzlvl1 z1 = new frm_zusatzlvl1(ben, id, leben1);
            z1.ShowDialog();
            Aktualisieren();
            leben1 = cls_spiel.Leben;
            Leben();
            pbx_zlvl1.Visible = false;
            pbx_2.Visible = false;
        }

        private void pbx_lvl3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aus dem Himmel fallen die verschiedensten Flaggen. Fange nur die Länder auf, die zu Afrika gehören. \nWenn du die falschen Länder berührst bekommst du Minuspunkte!");
            frm_level3 lvl3 = new frm_level3(ben, leben1, id);
            lvl3.ShowDialog();
            Aktualisieren();
            high3 = cls_spiel.Highscorelvl3;
            leben1 = cls_spiel.Leben;
            if (high3 > 150)
            {
                pbx_lvl4.Visible = true;
                pbx_4.Visible = true;
            }
            else if (high3 < 150)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 150 haben, um fortzufahren!");
            }
            Leben();
        }

        private void pbx_lvl4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ordne die Nationalhymnen den Ländern zu.", "Level 4 Bemerkung");
            frm_lvl4 l = new frm_lvl4(ben, leben1, id);
            l.ShowDialog();
            Aktualisieren();
            high4 = cls_spiel.Highscorelvl4;
            leben1 = cls_spiel.Leben;
            if (high4 > 60)
            {
                pbx_lvl5.Visible = true;
                pbx_5.Visible = true;
            }
            else if (high4 < 60)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 60 haben, um fortzufahren!");
            }
            Leben();
        }

        private void pbx_lvl5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Finde alle versteckten Länderumrisse im Bild. Achte auf die Zeit: Du hast nur 4 Minuten Zeit!", "Level 5 Bemerkung");
            frm_level5 l5 = new frm_level5(ben, leben1, id);
            l5.ShowDialog();
            Aktualisieren();
            high5 = cls_spiel.Highscorelvl5;
            leben1 = cls_spiel.Leben;
            if (high5 > 80)
            {
                pbx_lvl6.Visible = true;
                pbx_zlvl2.Visible = true;
                pbx_6.Visible = true;
                pbx_7.Visible = true;
            }
            else if (high6 < 80)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 80 haben, um fortzufahren!");
            }
            Leben();
        }

        private void pbx_zlvl2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Das hier ist ein Zusatzlevel mit schweren Fragen. Beantwortest du sie richtig, bekommst du ein Herz. Wenn du falsch antwortest kannst du aber kein Herz verlieren!");
            frm_zusatzlvl2 z2 = new frm_zusatzlvl2(ben, id, leben1);
            z2.ShowDialog();
            Aktualisieren();
            leben1 = cls_spiel.Leben;
            Leben();
            pbx_zlvl2.Visible = false;
            pbx_7.Visible = false;
        }

        private void pbx_lvl6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Klicke die Richtigen Gebiete auf dem Mars an! Du hast 5 Minuten Zeit.", "Level 6 Bemerkung");
            frm_level6 l = new frm_level6(ben, leben1, id);
            l.ShowDialog();
            Aktualisieren();
            high6 = cls_spiel.Highscorelvl6;
            leben1 = cls_spiel.Leben;
            if (high6 > 160)
            {
                MessageBox.Show("Du hast es geschafft!");
            }
            else if (high6 < 160)
            {
                MessageBox.Show("Sie müssen mindestens einen Highscore von 160 haben, um das Spiel beenden zu können!");
            }
            Leben();
        }
    }
}
