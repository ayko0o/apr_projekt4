﻿namespace semesterprojekt_1
{
    partial class frm_signup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_vorname = new System.Windows.Forms.Label();
            this.lbl_nachname = new System.Windows.Forms.Label();
            this.lbl_benutzer = new System.Windows.Forms.Label();
            this.lbl_passwort = new System.Windows.Forms.Label();
            this.lbl_geburtsdatum = new System.Windows.Forms.Label();
            this.lbl_adresse = new System.Windows.Forms.Label();
            this.lbl_ort = new System.Windows.Forms.Label();
            this.lbl_postleitzahl = new System.Windows.Forms.Label();
            this.lbl_familienstand = new System.Windows.Forms.Label();
            this.lbl_telefonnummer = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_blutgruppe = new System.Windows.Forms.Label();
            this.lbl_haarfarbe = new System.Windows.Forms.Label();
            this.lbl_augenfarbe = new System.Windows.Forms.Label();
            this.lbl_brille = new System.Windows.Forms.Label();
            this.lbl_willkommen = new System.Windows.Forms.Label();
            this.btn_loschen = new System.Windows.Forms.Button();
            this.btn_registrieren = new System.Windows.Forms.Button();
            this.btn_schliesen = new System.Windows.Forms.Button();
            this.rbtn_ja = new System.Windows.Forms.RadioButton();
            this.rbtn_nein = new System.Windows.Forms.RadioButton();
            this.dtp_geburtsdatum = new System.Windows.Forms.DateTimePicker();
            this.tbx_vorname = new System.Windows.Forms.TextBox();
            this.tbx_nachname = new System.Windows.Forms.TextBox();
            this.tbx_benutzer = new System.Windows.Forms.TextBox();
            this.tbx_passwort = new System.Windows.Forms.TextBox();
            this.tbx_adresse = new System.Windows.Forms.TextBox();
            this.tbx_ort = new System.Windows.Forms.TextBox();
            this.tbx_postleitzahl = new System.Windows.Forms.TextBox();
            this.tbx_familienstand = new System.Windows.Forms.TextBox();
            this.tbx_telefonnummer = new System.Windows.Forms.TextBox();
            this.tbx_email = new System.Windows.Forms.TextBox();
            this.tbx_blutgruppe = new System.Windows.Forms.TextBox();
            this.tbx_haarfarbe = new System.Windows.Forms.TextBox();
            this.tbx_augenfarbe = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_vorname
            // 
            this.lbl_vorname.AutoSize = true;
            this.lbl_vorname.Location = new System.Drawing.Point(43, 93);
            this.lbl_vorname.Name = "lbl_vorname";
            this.lbl_vorname.Size = new System.Drawing.Size(60, 15);
            this.lbl_vorname.TabIndex = 0;
            this.lbl_vorname.Text = "Vorname:";
            // 
            // lbl_nachname
            // 
            this.lbl_nachname.AutoSize = true;
            this.lbl_nachname.Location = new System.Drawing.Point(43, 121);
            this.lbl_nachname.Name = "lbl_nachname";
            this.lbl_nachname.Size = new System.Drawing.Size(71, 15);
            this.lbl_nachname.TabIndex = 0;
            this.lbl_nachname.Text = "Nachname:";
            // 
            // lbl_benutzer
            // 
            this.lbl_benutzer.AutoSize = true;
            this.lbl_benutzer.Location = new System.Drawing.Point(43, 154);
            this.lbl_benutzer.Name = "lbl_benutzer";
            this.lbl_benutzer.Size = new System.Drawing.Size(59, 15);
            this.lbl_benutzer.TabIndex = 0;
            this.lbl_benutzer.Text = "Benutzer:";
            // 
            // lbl_passwort
            // 
            this.lbl_passwort.AutoSize = true;
            this.lbl_passwort.Location = new System.Drawing.Point(43, 184);
            this.lbl_passwort.Name = "lbl_passwort";
            this.lbl_passwort.Size = new System.Drawing.Size(60, 15);
            this.lbl_passwort.TabIndex = 0;
            this.lbl_passwort.Text = "Passwort:";
            // 
            // lbl_geburtsdatum
            // 
            this.lbl_geburtsdatum.AutoSize = true;
            this.lbl_geburtsdatum.Location = new System.Drawing.Point(43, 213);
            this.lbl_geburtsdatum.Name = "lbl_geburtsdatum";
            this.lbl_geburtsdatum.Size = new System.Drawing.Size(88, 15);
            this.lbl_geburtsdatum.TabIndex = 0;
            this.lbl_geburtsdatum.Text = "Geburtsdatum:";
            // 
            // lbl_adresse
            // 
            this.lbl_adresse.AutoSize = true;
            this.lbl_adresse.Location = new System.Drawing.Point(43, 247);
            this.lbl_adresse.Name = "lbl_adresse";
            this.lbl_adresse.Size = new System.Drawing.Size(51, 15);
            this.lbl_adresse.TabIndex = 0;
            this.lbl_adresse.Text = "Adresse";
            // 
            // lbl_ort
            // 
            this.lbl_ort.AutoSize = true;
            this.lbl_ort.Location = new System.Drawing.Point(43, 278);
            this.lbl_ort.Name = "lbl_ort";
            this.lbl_ort.Size = new System.Drawing.Size(26, 15);
            this.lbl_ort.TabIndex = 0;
            this.lbl_ort.Text = "Ort:";
            // 
            // lbl_postleitzahl
            // 
            this.lbl_postleitzahl.AutoSize = true;
            this.lbl_postleitzahl.Location = new System.Drawing.Point(43, 312);
            this.lbl_postleitzahl.Name = "lbl_postleitzahl";
            this.lbl_postleitzahl.Size = new System.Drawing.Size(73, 15);
            this.lbl_postleitzahl.TabIndex = 0;
            this.lbl_postleitzahl.Text = "Postleitzahl:";
            // 
            // lbl_familienstand
            // 
            this.lbl_familienstand.AutoSize = true;
            this.lbl_familienstand.Location = new System.Drawing.Point(43, 344);
            this.lbl_familienstand.Name = "lbl_familienstand";
            this.lbl_familienstand.Size = new System.Drawing.Size(88, 15);
            this.lbl_familienstand.TabIndex = 0;
            this.lbl_familienstand.Text = "Familienstand:";
            // 
            // lbl_telefonnummer
            // 
            this.lbl_telefonnummer.AutoSize = true;
            this.lbl_telefonnummer.Location = new System.Drawing.Point(43, 376);
            this.lbl_telefonnummer.Name = "lbl_telefonnummer";
            this.lbl_telefonnummer.Size = new System.Drawing.Size(98, 15);
            this.lbl_telefonnummer.TabIndex = 0;
            this.lbl_telefonnummer.Text = "Telefonnummer:";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(43, 409);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(46, 15);
            this.lbl_email.TabIndex = 0;
            this.lbl_email.Text = "E-Mail:";
            // 
            // lbl_blutgruppe
            // 
            this.lbl_blutgruppe.AutoSize = true;
            this.lbl_blutgruppe.Location = new System.Drawing.Point(44, 443);
            this.lbl_blutgruppe.Name = "lbl_blutgruppe";
            this.lbl_blutgruppe.Size = new System.Drawing.Size(70, 15);
            this.lbl_blutgruppe.TabIndex = 0;
            this.lbl_blutgruppe.Text = "Blutgruppe:";
            // 
            // lbl_haarfarbe
            // 
            this.lbl_haarfarbe.AutoSize = true;
            this.lbl_haarfarbe.Location = new System.Drawing.Point(44, 477);
            this.lbl_haarfarbe.Name = "lbl_haarfarbe";
            this.lbl_haarfarbe.Size = new System.Drawing.Size(65, 15);
            this.lbl_haarfarbe.TabIndex = 0;
            this.lbl_haarfarbe.Text = "Haarfarbe:";
            // 
            // lbl_augenfarbe
            // 
            this.lbl_augenfarbe.AutoSize = true;
            this.lbl_augenfarbe.Location = new System.Drawing.Point(44, 510);
            this.lbl_augenfarbe.Name = "lbl_augenfarbe";
            this.lbl_augenfarbe.Size = new System.Drawing.Size(73, 15);
            this.lbl_augenfarbe.TabIndex = 0;
            this.lbl_augenfarbe.Text = "Augenfarbe:";
            // 
            // lbl_brille
            // 
            this.lbl_brille.AutoSize = true;
            this.lbl_brille.Location = new System.Drawing.Point(44, 539);
            this.lbl_brille.Name = "lbl_brille";
            this.lbl_brille.Size = new System.Drawing.Size(91, 15);
            this.lbl_brille.TabIndex = 0;
            this.lbl_brille.Text = "Brille (Ja/Nein):";
            // 
            // lbl_willkommen
            // 
            this.lbl_willkommen.AutoSize = true;
            this.lbl_willkommen.Font = new System.Drawing.Font("Elephant", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_willkommen.Location = new System.Drawing.Point(30, 22);
            this.lbl_willkommen.Name = "lbl_willkommen";
            this.lbl_willkommen.Size = new System.Drawing.Size(321, 40);
            this.lbl_willkommen.TabIndex = 0;
            this.lbl_willkommen.Text = "Willkommen! \r\nUm fortzufahren melden Sie sich bitte an.";
            this.lbl_willkommen.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn_loschen
            // 
            this.btn_loschen.Location = new System.Drawing.Point(47, 607);
            this.btn_loschen.Name = "btn_loschen";
            this.btn_loschen.Size = new System.Drawing.Size(122, 36);
            this.btn_loschen.TabIndex = 17;
            this.btn_loschen.Text = "Löschen";
            this.btn_loschen.UseVisualStyleBackColor = true;
            this.btn_loschen.Click += new System.EventHandler(this.btn_loschen_Click);
            // 
            // btn_registrieren
            // 
            this.btn_registrieren.Location = new System.Drawing.Point(208, 607);
            this.btn_registrieren.Name = "btn_registrieren";
            this.btn_registrieren.Size = new System.Drawing.Size(120, 36);
            this.btn_registrieren.TabIndex = 18;
            this.btn_registrieren.Text = "Registrieren";
            this.btn_registrieren.UseVisualStyleBackColor = true;
            this.btn_registrieren.Click += new System.EventHandler(this.btn_registrieren_Click);
            // 
            // btn_schliesen
            // 
            this.btn_schliesen.Location = new System.Drawing.Point(117, 661);
            this.btn_schliesen.Name = "btn_schliesen";
            this.btn_schliesen.Size = new System.Drawing.Size(141, 35);
            this.btn_schliesen.TabIndex = 19;
            this.btn_schliesen.Text = "Schließen";
            this.btn_schliesen.UseVisualStyleBackColor = true;
            this.btn_schliesen.Click += new System.EventHandler(this.btn_schliesen_Click);
            // 
            // rbtn_ja
            // 
            this.rbtn_ja.AutoSize = true;
            this.rbtn_ja.Location = new System.Drawing.Point(157, 539);
            this.rbtn_ja.Name = "rbtn_ja";
            this.rbtn_ja.Size = new System.Drawing.Size(41, 19);
            this.rbtn_ja.TabIndex = 15;
            this.rbtn_ja.TabStop = true;
            this.rbtn_ja.Text = "Ja";
            this.rbtn_ja.UseVisualStyleBackColor = true;
            // 
            // rbtn_nein
            // 
            this.rbtn_nein.AutoSize = true;
            this.rbtn_nein.Location = new System.Drawing.Point(157, 564);
            this.rbtn_nein.Name = "rbtn_nein";
            this.rbtn_nein.Size = new System.Drawing.Size(54, 19);
            this.rbtn_nein.TabIndex = 16;
            this.rbtn_nein.TabStop = true;
            this.rbtn_nein.Text = "Nein";
            this.rbtn_nein.UseVisualStyleBackColor = true;
            // 
            // dtp_geburtsdatum
            // 
            this.dtp_geburtsdatum.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_geburtsdatum.Location = new System.Drawing.Point(157, 213);
            this.dtp_geburtsdatum.Name = "dtp_geburtsdatum";
            this.dtp_geburtsdatum.Size = new System.Drawing.Size(171, 20);
            this.dtp_geburtsdatum.TabIndex = 5;
            // 
            // tbx_vorname
            // 
            this.tbx_vorname.Location = new System.Drawing.Point(157, 93);
            this.tbx_vorname.Name = "tbx_vorname";
            this.tbx_vorname.Size = new System.Drawing.Size(171, 20);
            this.tbx_vorname.TabIndex = 1;
            // 
            // tbx_nachname
            // 
            this.tbx_nachname.Location = new System.Drawing.Point(157, 121);
            this.tbx_nachname.Name = "tbx_nachname";
            this.tbx_nachname.Size = new System.Drawing.Size(171, 20);
            this.tbx_nachname.TabIndex = 2;
            // 
            // tbx_benutzer
            // 
            this.tbx_benutzer.Location = new System.Drawing.Point(157, 151);
            this.tbx_benutzer.Name = "tbx_benutzer";
            this.tbx_benutzer.Size = new System.Drawing.Size(171, 20);
            this.tbx_benutzer.TabIndex = 3;
            // 
            // tbx_passwort
            // 
            this.tbx_passwort.Location = new System.Drawing.Point(157, 181);
            this.tbx_passwort.Name = "tbx_passwort";
            this.tbx_passwort.PasswordChar = '*';
            this.tbx_passwort.Size = new System.Drawing.Size(171, 20);
            this.tbx_passwort.TabIndex = 4;
            // 
            // tbx_adresse
            // 
            this.tbx_adresse.Location = new System.Drawing.Point(157, 247);
            this.tbx_adresse.Name = "tbx_adresse";
            this.tbx_adresse.Size = new System.Drawing.Size(171, 20);
            this.tbx_adresse.TabIndex = 6;
            // 
            // tbx_ort
            // 
            this.tbx_ort.Location = new System.Drawing.Point(157, 278);
            this.tbx_ort.Name = "tbx_ort";
            this.tbx_ort.Size = new System.Drawing.Size(171, 20);
            this.tbx_ort.TabIndex = 7;
            // 
            // tbx_postleitzahl
            // 
            this.tbx_postleitzahl.Location = new System.Drawing.Point(157, 312);
            this.tbx_postleitzahl.Name = "tbx_postleitzahl";
            this.tbx_postleitzahl.Size = new System.Drawing.Size(171, 20);
            this.tbx_postleitzahl.TabIndex = 8;
            // 
            // tbx_familienstand
            // 
            this.tbx_familienstand.Location = new System.Drawing.Point(157, 344);
            this.tbx_familienstand.Name = "tbx_familienstand";
            this.tbx_familienstand.Size = new System.Drawing.Size(171, 20);
            this.tbx_familienstand.TabIndex = 9;
            // 
            // tbx_telefonnummer
            // 
            this.tbx_telefonnummer.Location = new System.Drawing.Point(157, 376);
            this.tbx_telefonnummer.Name = "tbx_telefonnummer";
            this.tbx_telefonnummer.Size = new System.Drawing.Size(171, 20);
            this.tbx_telefonnummer.TabIndex = 10;
            // 
            // tbx_email
            // 
            this.tbx_email.Location = new System.Drawing.Point(157, 409);
            this.tbx_email.Name = "tbx_email";
            this.tbx_email.Size = new System.Drawing.Size(171, 20);
            this.tbx_email.TabIndex = 11;
            // 
            // tbx_blutgruppe
            // 
            this.tbx_blutgruppe.Location = new System.Drawing.Point(157, 443);
            this.tbx_blutgruppe.Name = "tbx_blutgruppe";
            this.tbx_blutgruppe.Size = new System.Drawing.Size(171, 20);
            this.tbx_blutgruppe.TabIndex = 12;
            // 
            // tbx_haarfarbe
            // 
            this.tbx_haarfarbe.Location = new System.Drawing.Point(157, 477);
            this.tbx_haarfarbe.Name = "tbx_haarfarbe";
            this.tbx_haarfarbe.Size = new System.Drawing.Size(171, 20);
            this.tbx_haarfarbe.TabIndex = 13;
            // 
            // tbx_augenfarbe
            // 
            this.tbx_augenfarbe.Location = new System.Drawing.Point(157, 510);
            this.tbx_augenfarbe.Name = "tbx_augenfarbe";
            this.tbx_augenfarbe.Size = new System.Drawing.Size(171, 20);
            this.tbx_augenfarbe.TabIndex = 14;
            // 
            // frm_signup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(383, 719);
            this.Controls.Add(this.tbx_augenfarbe);
            this.Controls.Add(this.tbx_haarfarbe);
            this.Controls.Add(this.tbx_blutgruppe);
            this.Controls.Add(this.tbx_email);
            this.Controls.Add(this.tbx_telefonnummer);
            this.Controls.Add(this.tbx_familienstand);
            this.Controls.Add(this.tbx_postleitzahl);
            this.Controls.Add(this.tbx_ort);
            this.Controls.Add(this.tbx_adresse);
            this.Controls.Add(this.tbx_passwort);
            this.Controls.Add(this.tbx_benutzer);
            this.Controls.Add(this.tbx_nachname);
            this.Controls.Add(this.tbx_vorname);
            this.Controls.Add(this.dtp_geburtsdatum);
            this.Controls.Add(this.rbtn_nein);
            this.Controls.Add(this.rbtn_ja);
            this.Controls.Add(this.btn_schliesen);
            this.Controls.Add(this.btn_registrieren);
            this.Controls.Add(this.btn_loschen);
            this.Controls.Add(this.lbl_brille);
            this.Controls.Add(this.lbl_augenfarbe);
            this.Controls.Add(this.lbl_haarfarbe);
            this.Controls.Add(this.lbl_blutgruppe);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.lbl_telefonnummer);
            this.Controls.Add(this.lbl_familienstand);
            this.Controls.Add(this.lbl_postleitzahl);
            this.Controls.Add(this.lbl_ort);
            this.Controls.Add(this.lbl_adresse);
            this.Controls.Add(this.lbl_geburtsdatum);
            this.Controls.Add(this.lbl_passwort);
            this.Controls.Add(this.lbl_benutzer);
            this.Controls.Add(this.lbl_nachname);
            this.Controls.Add(this.lbl_willkommen);
            this.Controls.Add(this.lbl_vorname);
            this.Name = "frm_signup";
            this.Text = "Sign Up";
            this.Load += new System.EventHandler(this.frm_signup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_vorname;
        private System.Windows.Forms.Label lbl_nachname;
        private System.Windows.Forms.Label lbl_benutzer;
        private System.Windows.Forms.Label lbl_passwort;
        private System.Windows.Forms.Label lbl_geburtsdatum;
        private System.Windows.Forms.Label lbl_adresse;
        private System.Windows.Forms.Label lbl_ort;
        private System.Windows.Forms.Label lbl_postleitzahl;
        private System.Windows.Forms.Label lbl_familienstand;
        private System.Windows.Forms.Label lbl_telefonnummer;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_blutgruppe;
        private System.Windows.Forms.Label lbl_haarfarbe;
        private System.Windows.Forms.Label lbl_augenfarbe;
        private System.Windows.Forms.Label lbl_brille;
        private System.Windows.Forms.Label lbl_willkommen;
        private System.Windows.Forms.Button btn_loschen;
        private System.Windows.Forms.Button btn_registrieren;
        private System.Windows.Forms.Button btn_schliesen;
        private System.Windows.Forms.RadioButton rbtn_ja;
        private System.Windows.Forms.RadioButton rbtn_nein;
        private System.Windows.Forms.DateTimePicker dtp_geburtsdatum;
        private System.Windows.Forms.TextBox tbx_vorname;
        private System.Windows.Forms.TextBox tbx_nachname;
        private System.Windows.Forms.TextBox tbx_benutzer;
        private System.Windows.Forms.TextBox tbx_passwort;
        private System.Windows.Forms.TextBox tbx_adresse;
        private System.Windows.Forms.TextBox tbx_ort;
        private System.Windows.Forms.TextBox tbx_postleitzahl;
        private System.Windows.Forms.TextBox tbx_familienstand;
        private System.Windows.Forms.TextBox tbx_telefonnummer;
        private System.Windows.Forms.TextBox tbx_email;
        private System.Windows.Forms.TextBox tbx_blutgruppe;
        private System.Windows.Forms.TextBox tbx_haarfarbe;
        private System.Windows.Forms.TextBox tbx_augenfarbe;
    }
}