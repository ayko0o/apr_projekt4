﻿namespace semesterprojekt_1
{
    partial class frm_zusatzlvl1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_f1 = new System.Windows.Forms.Label();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.lbl_hs = new System.Windows.Forms.Label();
            this.lbl_falsch = new System.Windows.Forms.Label();
            this.lbl_falsch2 = new System.Windows.Forms.Label();
            this.lbl_falsch3 = new System.Windows.Forms.Label();
            this.lbl_fragea = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_f1
            // 
            this.lbl_f1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_f1.Location = new System.Drawing.Point(98, 142);
            this.lbl_f1.Name = "lbl_f1";
            this.lbl_f1.Size = new System.Drawing.Size(569, 35);
            this.lbl_f1.TabIndex = 0;
            this.lbl_f1.Text = "Wie heißt die Hauptstadt von Malaysia?";
            this.lbl_f1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_1
            // 
            this.btn_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
            this.btn_1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Location = new System.Drawing.Point(257, 226);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(260, 61);
            this.btn_1.TabIndex = 1;
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btn_2
            // 
            this.btn_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
            this.btn_2.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Location = new System.Drawing.Point(257, 309);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(260, 61);
            this.btn_2.TabIndex = 2;
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_3
            // 
            this.btn_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
            this.btn_3.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Location = new System.Drawing.Point(257, 391);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(260, 61);
            this.btn_3.TabIndex = 3;
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // lbl_hs
            // 
            this.lbl_hs.AutoSize = true;
            this.lbl_hs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_hs.Location = new System.Drawing.Point(12, 84);
            this.lbl_hs.Name = "lbl_hs";
            this.lbl_hs.Size = new System.Drawing.Size(80, 24);
            this.lbl_hs.TabIndex = 4;
            this.lbl_hs.Text = "Punkte:";
            // 
            // lbl_falsch
            // 
            this.lbl_falsch.AutoSize = true;
            this.lbl_falsch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.lbl_falsch.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_falsch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch.Location = new System.Drawing.Point(534, 245);
            this.lbl_falsch.Name = "lbl_falsch";
            this.lbl_falsch.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch.TabIndex = 5;
            this.lbl_falsch.Text = "Falsch";
            this.lbl_falsch.Visible = false;
            // 
            // lbl_falsch2
            // 
            this.lbl_falsch2.AutoSize = true;
            this.lbl_falsch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.lbl_falsch2.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_falsch2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch2.Location = new System.Drawing.Point(534, 328);
            this.lbl_falsch2.Name = "lbl_falsch2";
            this.lbl_falsch2.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch2.TabIndex = 6;
            this.lbl_falsch2.Text = "Falsch";
            this.lbl_falsch2.Visible = false;
            // 
            // lbl_falsch3
            // 
            this.lbl_falsch3.AutoSize = true;
            this.lbl_falsch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.lbl_falsch3.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_falsch3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch3.Location = new System.Drawing.Point(534, 410);
            this.lbl_falsch3.Name = "lbl_falsch3";
            this.lbl_falsch3.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch3.TabIndex = 7;
            this.lbl_falsch3.Text = "Falsch";
            this.lbl_falsch3.Visible = false;
            // 
            // lbl_fragea
            // 
            this.lbl_fragea.AutoSize = true;
            this.lbl_fragea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.lbl_fragea.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fragea.Location = new System.Drawing.Point(296, 28);
            this.lbl_fragea.Name = "lbl_fragea";
            this.lbl_fragea.Size = new System.Drawing.Size(139, 24);
            this.lbl_fragea.TabIndex = 13;
            this.lbl_fragea.Text = "Frage 1 von 5";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.pictureBox5.Location = new System.Drawing.Point(-28, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(807, 56);
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::semesterprojekt_1.Properties.Resources.froggy;
            this.pictureBox1.Location = new System.Drawing.Point(98, 483);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(44, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::semesterprojekt_1.Properties.Resources.baum;
            this.pictureBox2.Location = new System.Drawing.Point(-48, 291);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(213, 305);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.pictureBox3.Location = new System.Drawing.Point(72, 127);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(620, 63);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(232)))), ((int)(((byte)(227)))));
            this.pictureBox4.Location = new System.Drawing.Point(221, 210);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(385, 263);
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // frm_zusatzlvl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
            this.ClientSize = new System.Drawing.Size(768, 576);
            this.Controls.Add(this.lbl_fragea);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.lbl_hs);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lbl_falsch3);
            this.Controls.Add(this.lbl_falsch2);
            this.Controls.Add(this.lbl_falsch);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.lbl_f1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Name = "frm_zusatzlvl1";
            this.Text = "Zusatzlevel 1";
            this.Load += new System.EventHandler(this.frm_zusatzlvl1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_f1;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Label lbl_hs;
        private System.Windows.Forms.Label lbl_falsch;
        private System.Windows.Forms.Label lbl_falsch2;
        private System.Windows.Forms.Label lbl_falsch3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label lbl_fragea;
    }
}