using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace semesterprojekt_1
{
    public partial class frm_level1 : Form
    {
        int highscore;
        int gesamtzeit;
        int count = 0;
        int leben;
        string ben;
        int id;
        int level = 1;
        bool lvl1;
        List<string> eu = new List<string>();

        public frm_level1()
        {
            InitializeComponent();
        }
        public frm_level1(string benutzer, int lb, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lb;
            id = idd;
        }

        private void tmr_lvl1_Tick(object sender, EventArgs e)
        {
            lbl_lvl1_zeit.Text = "Zeit:" + gesamtzeit--.ToString();

            if (gesamtzeit < 1)
            {
                tmr_lvl1.Stop();
                MessageBox.Show("Du hast es nicht geschafft.", "Level 1");
            }

        }

        private void frm_level1_Load(object sender, EventArgs e)
        {
            Excel();
            gesamtzeit = 180;
            tmr_lvl1.Start();
        }

        public void btn_lvl1_aufgeben_Click(object sender, EventArgs e)
        {
            lvl1 = true;
            tmr_lvl1.Stop();
            lbl_lvl1_zeit.Text = "Zeit: 0";
            count = 0;
            lbl_lvl1_count.Text = string.Format("0/27");
            MessageBox.Show("Du verlierst 1 Herz.", "Level 1 aufgegeben");

            leben++;
            cls_spiel.Leben = leben;
            cls_spiel.Level = level;
            cls_spiel.Main = lvl1;

            this.Close();
            
        }

        private void Excel()
        {
            string file = "C:/lander.xlsx";
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb;
            Worksheet ws;

            wb = excel.Workbooks.Open(file);
            ws = excel.Worksheets[1];


            Range zelle = ws.Range["A1:A27"];

            foreach (string ergebnis in zelle.Value)
            {
                eu.Add(ergebnis);

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            Spiel();
            
        }

       
        private void Spiel()
        {
            Spiel2();
            for (int i = 0; i < eu.Count; i++)
            {
                if (eu[i] == tbx_eingabe.Text)
                {
                    count++;
                    lbl_lvl1_count.Text = string.Format("{0}/27", count);
                    tbx_eingabe.Text = "";
                    eu.Remove(eu[i]);
                    if (count == 27)
                    {
                        highscore = gesamtzeit + count;
                        tmr_lvl1.Stop();
                        cls_dataprovider.Highscore(ben, highscore, level, id);
                        cls_spiel.Leben = leben;
                        cls_spiel.Highscore = highscore;
                        cls_spiel.Level = level;
                        cls_spiel.Highscorelvl1 = highscore;
                        MessageBox.Show("Du hast alle EU-Staaten gefunden! \n\nDein Highscore liegt momentan bei " + highscore + " Punkten.", "Level 1 abgeschlossen");
                        
                        this.Close();

                    }
                }
            }
            
            
        }
        private void Spiel2()
        {
            if(tbx_eingabe.Text == "Deutschland")
            {
                p1.Visible = true;
            }
            else if(tbx_eingabe.Text =="Belgien")
            {
                p2.Visible = true;
            }
            else if (tbx_eingabe.Text == "Bulgarien")
            {
                p3.Visible = true;
            }
            else if (tbx_eingabe.Text == "Dänemark")
            {
                p4.Visible = true;
            }
            else if (tbx_eingabe.Text == "Estland")
            {
                p5.Visible = true;
            }
            else if (tbx_eingabe.Text == "Finnland")
            {
                p6.Visible = true;
            }
            else if (tbx_eingabe.Text == "Frankreich")
            {
                p7.Visible = true;
            }
            else if (tbx_eingabe.Text == "Griechenland")
            {
                p8.Visible = true;
            }
            else if (tbx_eingabe.Text == "Irland")
            {
                p9.Visible = true;
            }
            else if (tbx_eingabe.Text == "Italien")
            {
                p10.Visible = true;
            }
            else if (tbx_eingabe.Text == "Kroatien")
            {
                p11.Visible = true;
            }
            else if (tbx_eingabe.Text == "Lettland")
            {
                p12.Visible = true;
            }
            else if (tbx_eingabe.Text == "Litauen")
            {
                p13.Visible = true;
            }
            else if (tbx_eingabe.Text == "Luxemburg")
            {
                p14.Visible = true;
            }
            else if (tbx_eingabe.Text == "Malta")
            {
                p15.Visible = true;
            }
            else if (tbx_eingabe.Text == "Niederlande")
            {
                p16.Visible = true;
            }
            else if (tbx_eingabe.Text == "Österreich")
            {
                p17.Visible = true;
            }
            else if (tbx_eingabe.Text == "Polen")
            {
                p18.Visible = true;
            }
            else if (tbx_eingabe.Text == "Portugal")
            {
                p19.Visible = true;
            }
            else if (tbx_eingabe.Text == "Rumänien")
            {
                p20.Visible = true;
            }
            else if (tbx_eingabe.Text == "Schweden")
            {
                p21.Visible = true;
            }
            else if (tbx_eingabe.Text == "Slowakei")
            {
                p22.Visible = true;
            }
            else if (tbx_eingabe.Text == "Slowenien")
            {
                p23.Visible = true;
            }
            else if (tbx_eingabe.Text == "Spanien")
            {
                p24.Visible = true;
            }
            else if (tbx_eingabe.Text == "Tschechien")
            {
                p25.Visible = true;
            }
            else if (tbx_eingabe.Text == "Ungarn")
            {
                p26.Visible = true;
            }
            else if (tbx_eingabe.Text == "Zypern")
            {
                p27.Visible = true;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
