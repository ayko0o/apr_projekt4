﻿using System;

namespace semesterprojekt_1
{
    internal class cls_konto
    {
        string m_vorname;
        string m_nachname;
        string m_benutzer;
        string m_passwort;
        DateTime m_geburtsdatum;
        string m_adresse;
        string m_ort;
        string m_postleitzahl;
        string m_familienstatus;
        string m_telefonnummer;
        string m_email;
        string m_blutgruppe;
        string m_haarfarbe;
        string m_augenfarbe;
        string m_brille;
        int m_highscore;
        DateTime m_zeit;
        int m_level;

        public string Benutzer { get => m_benutzer; set => m_benutzer = value; }
        public string Passwort { get => m_passwort; set => m_passwort = value; }
        public string Vorname { get => m_vorname; set => m_vorname = value; }
        public string Nachname { get => m_nachname; set => m_nachname = value; }
        public DateTime Geburtsdatum { get => m_geburtsdatum; set => m_geburtsdatum = value; }
        public string Adresse { get => m_adresse; set => m_adresse = value; }
        public string Ort { get => m_ort; set => m_ort = value; }
        public string Postleitzahl { get => m_postleitzahl; set => m_postleitzahl = value; }
        public string Familienstatus { get => m_familienstatus; set => m_familienstatus = value; }
        public string Telefonnummer { get => m_telefonnummer; set => m_telefonnummer = value; }
        public string Email { get => m_email; set => m_email = value; }
        public string Blutgruppe { get => m_blutgruppe; set => m_blutgruppe = value; }
        public string Haarfarbe { get => m_haarfarbe; set => m_haarfarbe = value; }
        public string Augenfarbe { get => m_augenfarbe; set => m_augenfarbe = value; }
        public string Brille { get => m_brille; set => m_brille = value; }
        public int Highscore { get => m_highscore; set => m_highscore = value; }
        public DateTime Zeit { get => m_zeit; set => m_zeit = value; }
        public int Level { get => m_level; set => m_level = value; }

        public cls_konto(string benutzer, string passwort)
        {
            m_benutzer = benutzer;
            m_passwort = passwort;
        }
        public cls_konto(string benutzer, int highscore, int level, string passwort)
        {
            m_benutzer = benutzer;
            m_highscore = highscore;
            m_level = level;
            m_passwort = passwort;
        }
        public cls_konto()
        {
        }
        public cls_konto(string vorname, string nachname, string benutzer, string passwort, DateTime geburtsdatum, string adresse, string ort, string postleitzahl, string familienstand, string telefonnummer, string email, string blutgruppe, string haarfarbe, string augenfarbe, string brille)
        {
            m_vorname = vorname;
            m_nachname = nachname;
            m_benutzer = benutzer;
            m_passwort = passwort;
            m_geburtsdatum = geburtsdatum;
            m_adresse = adresse;
            m_ort = ort;
            m_postleitzahl = postleitzahl;
            m_familienstatus = familienstand;
            m_email = email;
            m_telefonnummer = telefonnummer;
            m_blutgruppe = blutgruppe;
            m_haarfarbe = haarfarbe;
            m_augenfarbe = augenfarbe;
            m_brille = brille;
        }

        public string Ausgabe
        {
            get
            {
                return string.Format("{0}   Highscore: {1}   Level: {2}", m_benutzer, m_highscore, m_level);
            }
        }


    }
}
