﻿namespace semesterprojekt_1
{
    partial class frm_level5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_aufgeben = new System.Windows.Forms.Button();
            this.lbl_aufgabe = new System.Windows.Forms.Label();
            this.tbx_eingabe = new System.Windows.Forms.TextBox();
            this.lbl_count = new System.Windows.Forms.Label();
            this.tmr_lvl5 = new System.Windows.Forms.Timer(this.components);
            this.lbl_zeit = new System.Windows.Forms.Label();
            this.lbl_highscore = new System.Windows.Forms.Label();
            this.pb_r = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_aufgeben
            // 
            this.btn_aufgeben.BackColor = System.Drawing.Color.Navy;
            this.btn_aufgeben.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_aufgeben.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_aufgeben.Location = new System.Drawing.Point(947, 308);
            this.btn_aufgeben.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_aufgeben.Name = "btn_aufgeben";
            this.btn_aufgeben.Size = new System.Drawing.Size(223, 55);
            this.btn_aufgeben.TabIndex = 2;
            this.btn_aufgeben.Text = "Aufgeben";
            this.btn_aufgeben.UseVisualStyleBackColor = false;
            this.btn_aufgeben.Click += new System.EventHandler(this.btn_aufgeben_Click);
            // 
            // lbl_aufgabe
            // 
            this.lbl_aufgabe.AutoSize = true;
            this.lbl_aufgabe.BackColor = System.Drawing.Color.Transparent;
            this.lbl_aufgabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_aufgabe.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_aufgabe.Location = new System.Drawing.Point(668, 62);
            this.lbl_aufgabe.Name = "lbl_aufgabe";
            this.lbl_aufgabe.Size = new System.Drawing.Size(265, 64);
            this.lbl_aufgabe.TabIndex = 2;
            this.lbl_aufgabe.Text = "Finde alle \r\nversteckte Länder!";
            this.lbl_aufgabe.Click += new System.EventHandler(this.lbl_aufgabe_Click);
            // 
            // tbx_eingabe
            // 
            this.tbx_eingabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx_eingabe.Location = new System.Drawing.Point(465, 268);
            this.tbx_eingabe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbx_eingabe.Name = "tbx_eingabe";
            this.tbx_eingabe.Size = new System.Drawing.Size(232, 38);
            this.tbx_eingabe.TabIndex = 1;
            this.tbx_eingabe.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.BackColor = System.Drawing.Color.Transparent;
            this.lbl_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_count.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_count.Location = new System.Drawing.Point(645, 318);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(51, 25);
            this.lbl_count.TabIndex = 4;
            this.lbl_count.Text = "0/11";
            this.lbl_count.Click += new System.EventHandler(this.lbl_count_Click);
            // 
            // tmr_lvl5
            // 
            this.tmr_lvl5.Interval = 1000;
            this.tmr_lvl5.Tick += new System.EventHandler(this.tmr_lvl5_Tick);
            // 
            // lbl_zeit
            // 
            this.lbl_zeit.AutoSize = true;
            this.lbl_zeit.BackColor = System.Drawing.Color.Navy;
            this.lbl_zeit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_zeit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_zeit.Location = new System.Drawing.Point(12, 249);
            this.lbl_zeit.Name = "lbl_zeit";
            this.lbl_zeit.Size = new System.Drawing.Size(125, 32);
            this.lbl_zeit.TabIndex = 5;
            this.lbl_zeit.Text = "Zeit: 240";
            // 
            // lbl_highscore
            // 
            this.lbl_highscore.AutoSize = true;
            this.lbl_highscore.BackColor = System.Drawing.Color.Navy;
            this.lbl_highscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_highscore.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_highscore.Location = new System.Drawing.Point(12, 286);
            this.lbl_highscore.Name = "lbl_highscore";
            this.lbl_highscore.Size = new System.Drawing.Size(173, 32);
            this.lbl_highscore.TabIndex = 6;
            this.lbl_highscore.Text = "Highscore: 0";
            // 
            // pb_r
            // 
            this.pb_r.Image = global::semesterprojekt_1.Properties.Resources.froggy3;
            this.pb_r.Location = new System.Drawing.Point(216, 249);
            this.pb_r.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_r.Name = "pb_r";
            this.pb_r.Size = new System.Drawing.Size(63, 64);
            this.pb_r.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_r.TabIndex = 9;
            this.pb_r.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Navy;
            this.pictureBox2.Location = new System.Drawing.Point(-8, 222);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(217, 121);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::semesterprojekt_1.Properties.Resources.space1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(-17, -2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1263, 732);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // frm_level5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1219, 721);
            this.Controls.Add(this.pb_r);
            this.Controls.Add(this.lbl_highscore);
            this.Controls.Add(this.lbl_zeit);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.tbx_eingabe);
            this.Controls.Add(this.lbl_aufgabe);
            this.Controls.Add(this.btn_aufgeben);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm_level5";
            this.Text = "Level 5";
            this.Load += new System.EventHandler(this.frm_level5_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_aufgeben;
        private System.Windows.Forms.Label lbl_aufgabe;
        private System.Windows.Forms.TextBox tbx_eingabe;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Timer tmr_lvl5;
        private System.Windows.Forms.Label lbl_zeit;
        private System.Windows.Forms.Label lbl_highscore;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pb_r;
    }
}