﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_zusatzlvl1 : Form
    {
        
        public frm_zusatzlvl1()
        {
            InitializeComponent();
        }
        public frm_zusatzlvl1(string benutzer, int idd, int leben)
        {
            InitializeComponent();
            ben = benutzer;
            id = idd;
            this.leben = leben;
        }

        int richtig;
        int a;
        int frage;
        int level = 7;
        string ben;
        int id;
        bool lvlz1;
        int leben;

        Random r = new Random();
        private void frm_zusatzlvl1_Load(object sender, EventArgs e)
        {
            Spiel();
            frage = 0;
            lbl_hs.Text = "Highscore: " + richtig;
        }
        private void Spiel()
        {
            if (frage == 0)
            {
                lbl_fragea.Text = "Frage 1 von 5";
                lbl_f1.Text = "Wie heißt die Hauptstadt von Malaysia?";
                btn_1.Text = "Kuala Lumpur";
                btn_2.Text = "Jakarta";
                btn_3.Text = "Maputo";
                if (a == 1)
                {
                    richtig = richtig + 30;
                    a = 0;
                    frage++;
                }
                else if(a ==2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;
                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;
                }
            }
            if (frage== 1)
            {
                lbl_fragea.Text = "Frage 2 von 5";
                lbl_falsch2.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_f1.Text = "Wie heißt die Hauptstadt von Polen?";
                btn_1.Text = "Minsk";
                btn_2.Text = "Maputo";
                btn_3.Text = "Warschau";
                if (a == 3)
                {
                    frage++;
                    a = 0;
                    richtig = richtig + 30;

                }
                else if(a == 1)
                {
                    richtig = richtig - 20;
                    lbl_falsch.Visible = true;

                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;

                }
            }
            if (frage == 2)
            {
                lbl_fragea.Text = "Frage 3 von 5";
                lbl_falsch2.Visible = false;
                lbl_falsch.Visible = false;
                lbl_f1.Text = "Baku ist die Hauptstadt welchen Landes?";
                btn_1.Text = "Aserbaidschan";
                btn_2.Text = "Nordmazedonien";
                btn_3.Text = "Kirgisistan";
                if (a == 1)
                {
                    frage++;
                    a = 0;
                    richtig = richtig + 30;

                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;

                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;

                }
            }
            if (frage == 3)
            {
                lbl_fragea.Text = "Frage 4 von 5";
                lbl_falsch2.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_f1.Text = "Wie heißt die Hauptstadt von Kambodscha?";
                btn_1.Text = "Skopje";
                btn_2.Text = "Phnom Penh";
                btn_3.Text = "Belgrad";
                if (a ==2)
                {
                    frage++;
                    a = 0;
                    richtig = richtig + 30;

                }
                else if (a == 1)
                {
                    richtig = richtig - 20;
                    lbl_falsch.Visible = true;

                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;

                }
            }
            if (frage == 4)
            {
                lbl_fragea.Text = "Frage 5 von 5";
                lbl_falsch.Visible = false;
                lbl_falsch3.Visible = false;
                lbl_f1.Text = "Wie heißt die Hauptstadt von Saudi-Arabien?";
                btn_1.Text = "Riad";
                btn_2.Text = "Baku";
                btn_3.Text = "Lima";
                if (a == 1)
                {
                    frage++;
                    a = 0;
                    richtig = richtig + 30;

                }
                else if (a == 2)
                {
                    richtig = richtig - 20;
                    lbl_falsch2.Visible = true;

                }
                else if (a == 3)
                {
                    richtig = richtig - 20;
                    lbl_falsch3.Visible = true;

                }
               
            }
            if (frage == 5)
            {
                if(richtig ==150)
                {
                    MessageBox.Show("Du hast alle Fragen richtig beantwortet, dafür bekommst du ein Herz. Dein Highscore für dieses Zusatzlevel liegt bei "+ richtig +" Punkten.", "Zusatzlevel 1 geschafft!!!");
                    lvlz1 = true;
                    cls_dataprovider.Highscore(ben, richtig, level, id);
                    leben--;
                    cls_spiel.Main = lvlz1;
                    cls_spiel.Highscore = richtig;
                    cls_spiel.Level = level;
                    cls_spiel.Leben = leben;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Du hast alle Fragen beantwortet. Leider waren nicht alle richtig, du bekommst also kein Herz. Dein Highscore für dieses Zusatzlevel liegt bei " + richtig + " Punkten.", "Zusatzlevel 1 geschafft!!!");
                    lvlz1 = true;
                    cls_dataprovider.Highscore(ben, richtig, level, id);
                    cls_spiel.Main = lvlz1;
                    cls_spiel.Highscore = richtig;
                    cls_spiel.Level = level;
                    cls_spiel.Leben = leben;
                    this.Close();
                }
               
            }
            lbl_hs.Text = "Highscore: " + richtig;
           
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            a = 1;
            Spiel();
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            a = 2;
            Spiel();
         
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            a = 3;
            Spiel();
       
        }
    }
}
