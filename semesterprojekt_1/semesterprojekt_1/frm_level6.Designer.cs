﻿namespace semesterprojekt_1
{
    partial class frm_level6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_gebiete = new System.Windows.Forms.Label();
            this.lbl_high = new System.Windows.Forms.Label();
            this.tmr_lvl6 = new System.Windows.Forms.Timer(this.components);
            this.lbl_zeit = new System.Windows.Forms.Label();
            this.btn_aufgeben = new System.Windows.Forms.Button();
            this.pb_r = new System.Windows.Forms.PictureBox();
            this.pbx_elysium = new System.Windows.Forms.PictureBox();
            this.pbx_utopia = new System.Windows.Forms.PictureBox();
            this.pbx_isidis = new System.Windows.Forms.PictureBox();
            this.pbx_hellas = new System.Windows.Forms.PictureBox();
            this.pbx_argyre = new System.Windows.Forms.PictureBox();
            this.pbx_noachis = new System.Windows.Forms.PictureBox();
            this.pbx_arabia = new System.Windows.Forms.PictureBox();
            this.pbx_vasitas = new System.Windows.Forms.PictureBox();
            this.pbx_chryse = new System.Windows.Forms.PictureBox();
            this.pbx_tempe = new System.Windows.Forms.PictureBox();
            this.pbx_alba = new System.Windows.Forms.PictureBox();
            this.pbx_amazonis = new System.Windows.Forms.PictureBox();
            this.pbx_olympus = new System.Windows.Forms.PictureBox();
            this.pbx_tharsism = new System.Windows.Forms.PictureBox();
            this.pbx_valles = new System.Windows.Forms.PictureBox();
            this.pbx_tharsisi = new System.Windows.Forms.PictureBox();
            this.pbx_abfluss = new System.Windows.Forms.PictureBox();
            this.pbx_south = new System.Windows.Forms.PictureBox();
            this.pbx_north = new System.Windows.Forms.PictureBox();
            this.pbx_mars = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_elysium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_utopia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_isidis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_hellas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_argyre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_noachis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_arabia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_vasitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_chryse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tempe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_alba)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_amazonis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_olympus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tharsism)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_valles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tharsisi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_abfluss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_south)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_north)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_mars)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_gebiete
            // 
            this.lbl_gebiete.AutoSize = true;
            this.lbl_gebiete.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gebiete.Location = new System.Drawing.Point(81, 30);
            this.lbl_gebiete.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gebiete.Name = "lbl_gebiete";
            this.lbl_gebiete.Size = new System.Drawing.Size(106, 24);
            this.lbl_gebiete.TabIndex = 1;
            this.lbl_gebiete.Text = "Klicke auf ";
            // 
            // lbl_high
            // 
            this.lbl_high.AutoSize = true;
            this.lbl_high.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_high.Location = new System.Drawing.Point(633, 28);
            this.lbl_high.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_high.Name = "lbl_high";
            this.lbl_high.Size = new System.Drawing.Size(98, 20);
            this.lbl_high.TabIndex = 4;
            this.lbl_high.Text = "Highscore: 0";
            // 
            // tmr_lvl6
            // 
            this.tmr_lvl6.Interval = 1000;
            this.tmr_lvl6.Tick += new System.EventHandler(this.tmr_lvl6_Tick);
            // 
            // lbl_zeit
            // 
            this.lbl_zeit.AutoSize = true;
            this.lbl_zeit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_zeit.Location = new System.Drawing.Point(636, 52);
            this.lbl_zeit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_zeit.Name = "lbl_zeit";
            this.lbl_zeit.Size = new System.Drawing.Size(71, 20);
            this.lbl_zeit.TabIndex = 7;
            this.lbl_zeit.Text = "Zeit: 300";
            // 
            // btn_aufgeben
            // 
            this.btn_aufgeben.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_aufgeben.Location = new System.Drawing.Point(523, 34);
            this.btn_aufgeben.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_aufgeben.Name = "btn_aufgeben";
            this.btn_aufgeben.Size = new System.Drawing.Size(90, 37);
            this.btn_aufgeben.TabIndex = 8;
            this.btn_aufgeben.Text = "Aufgeben";
            this.btn_aufgeben.UseVisualStyleBackColor = true;
            this.btn_aufgeben.Click += new System.EventHandler(this.btn_aufgeben_Click);
            // 
            // pb_r
            // 
            this.pb_r.Image = global::semesterprojekt_1.Properties.Resources.frog5;
            this.pb_r.Location = new System.Drawing.Point(10, 433);
            this.pb_r.Name = "pb_r";
            this.pb_r.Size = new System.Drawing.Size(72, 92);
            this.pb_r.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_r.TabIndex = 9;
            this.pb_r.TabStop = false;
            // 
            // pbx_elysium
            // 
            this.pbx_elysium.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_elysium.Location = new System.Drawing.Point(764, 257);
            this.pbx_elysium.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_elysium.Name = "pbx_elysium";
            this.pbx_elysium.Size = new System.Drawing.Size(14, 15);
            this.pbx_elysium.TabIndex = 6;
            this.pbx_elysium.TabStop = false;
            this.pbx_elysium.Click += new System.EventHandler(this.pbx_elysium_Click);
            // 
            // pbx_utopia
            // 
            this.pbx_utopia.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_utopia.Location = new System.Drawing.Point(696, 212);
            this.pbx_utopia.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_utopia.Name = "pbx_utopia";
            this.pbx_utopia.Size = new System.Drawing.Size(14, 15);
            this.pbx_utopia.TabIndex = 6;
            this.pbx_utopia.TabStop = false;
            this.pbx_utopia.Click += new System.EventHandler(this.pbx_utopia_Click);
            // 
            // pbx_isidis
            // 
            this.pbx_isidis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_isidis.Location = new System.Drawing.Point(639, 273);
            this.pbx_isidis.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_isidis.Name = "pbx_isidis";
            this.pbx_isidis.Size = new System.Drawing.Size(14, 15);
            this.pbx_isidis.TabIndex = 6;
            this.pbx_isidis.TabStop = false;
            this.pbx_isidis.Click += new System.EventHandler(this.pbx_isidis_Click);
            // 
            // pbx_hellas
            // 
            this.pbx_hellas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_hellas.Location = new System.Drawing.Point(586, 407);
            this.pbx_hellas.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_hellas.Name = "pbx_hellas";
            this.pbx_hellas.Size = new System.Drawing.Size(14, 15);
            this.pbx_hellas.TabIndex = 6;
            this.pbx_hellas.TabStop = false;
            this.pbx_hellas.Click += new System.EventHandler(this.pbx_hellas_Click);
            // 
            // pbx_argyre
            // 
            this.pbx_argyre.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_argyre.Location = new System.Drawing.Point(344, 417);
            this.pbx_argyre.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_argyre.Name = "pbx_argyre";
            this.pbx_argyre.Size = new System.Drawing.Size(14, 15);
            this.pbx_argyre.TabIndex = 6;
            this.pbx_argyre.TabStop = false;
            this.pbx_argyre.Click += new System.EventHandler(this.pbx_argyre_Click);
            // 
            // pbx_noachis
            // 
            this.pbx_noachis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_noachis.Location = new System.Drawing.Point(456, 350);
            this.pbx_noachis.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_noachis.Name = "pbx_noachis";
            this.pbx_noachis.Size = new System.Drawing.Size(14, 15);
            this.pbx_noachis.TabIndex = 6;
            this.pbx_noachis.TabStop = false;
            this.pbx_noachis.Click += new System.EventHandler(this.pbx_noachis_Click);
            // 
            // pbx_arabia
            // 
            this.pbx_arabia.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_arabia.Location = new System.Drawing.Point(480, 249);
            this.pbx_arabia.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_arabia.Name = "pbx_arabia";
            this.pbx_arabia.Size = new System.Drawing.Size(14, 15);
            this.pbx_arabia.TabIndex = 6;
            this.pbx_arabia.TabStop = false;
            this.pbx_arabia.Click += new System.EventHandler(this.pbx_arabia_Click);
            // 
            // pbx_vasitas
            // 
            this.pbx_vasitas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_vasitas.Location = new System.Drawing.Point(480, 162);
            this.pbx_vasitas.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_vasitas.Name = "pbx_vasitas";
            this.pbx_vasitas.Size = new System.Drawing.Size(14, 15);
            this.pbx_vasitas.TabIndex = 6;
            this.pbx_vasitas.TabStop = false;
            this.pbx_vasitas.Click += new System.EventHandler(this.pbx_vasitas_Click);
            // 
            // pbx_chryse
            // 
            this.pbx_chryse.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_chryse.Location = new System.Drawing.Point(356, 219);
            this.pbx_chryse.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_chryse.Name = "pbx_chryse";
            this.pbx_chryse.Size = new System.Drawing.Size(14, 15);
            this.pbx_chryse.TabIndex = 6;
            this.pbx_chryse.TabStop = false;
            this.pbx_chryse.Click += new System.EventHandler(this.pbx_chryse_Click);
            // 
            // pbx_tempe
            // 
            this.pbx_tempe.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_tempe.Location = new System.Drawing.Point(262, 219);
            this.pbx_tempe.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_tempe.Name = "pbx_tempe";
            this.pbx_tempe.Size = new System.Drawing.Size(14, 15);
            this.pbx_tempe.TabIndex = 6;
            this.pbx_tempe.TabStop = false;
            this.pbx_tempe.Click += new System.EventHandler(this.pbx_tempe_Click);
            // 
            // pbx_alba
            // 
            this.pbx_alba.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_alba.Location = new System.Drawing.Point(192, 219);
            this.pbx_alba.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_alba.Name = "pbx_alba";
            this.pbx_alba.Size = new System.Drawing.Size(14, 15);
            this.pbx_alba.TabIndex = 6;
            this.pbx_alba.TabStop = false;
            this.pbx_alba.Click += new System.EventHandler(this.pbx_alba_Click);
            // 
            // pbx_amazonis
            // 
            this.pbx_amazonis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_amazonis.Location = new System.Drawing.Point(103, 212);
            this.pbx_amazonis.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_amazonis.Name = "pbx_amazonis";
            this.pbx_amazonis.Size = new System.Drawing.Size(14, 15);
            this.pbx_amazonis.TabIndex = 6;
            this.pbx_amazonis.TabStop = false;
            this.pbx_amazonis.Click += new System.EventHandler(this.pbx_amazonis_Click);
            // 
            // pbx_olympus
            // 
            this.pbx_olympus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_olympus.Location = new System.Drawing.Point(143, 265);
            this.pbx_olympus.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_olympus.Name = "pbx_olympus";
            this.pbx_olympus.Size = new System.Drawing.Size(14, 15);
            this.pbx_olympus.TabIndex = 6;
            this.pbx_olympus.TabStop = false;
            this.pbx_olympus.Click += new System.EventHandler(this.pbx_olympus_Click);
            // 
            // pbx_tharsism
            // 
            this.pbx_tharsism.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_tharsism.Location = new System.Drawing.Point(192, 306);
            this.pbx_tharsism.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_tharsism.Name = "pbx_tharsism";
            this.pbx_tharsism.Size = new System.Drawing.Size(14, 15);
            this.pbx_tharsism.TabIndex = 6;
            this.pbx_tharsism.TabStop = false;
            this.pbx_tharsism.Click += new System.EventHandler(this.pbx_tharsism_Click);
            // 
            // pbx_valles
            // 
            this.pbx_valles.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_valles.Location = new System.Drawing.Point(306, 340);
            this.pbx_valles.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_valles.Name = "pbx_valles";
            this.pbx_valles.Size = new System.Drawing.Size(14, 15);
            this.pbx_valles.TabIndex = 6;
            this.pbx_valles.TabStop = false;
            this.pbx_valles.Click += new System.EventHandler(this.pbx_valles_Click);
            // 
            // pbx_tharsisi
            // 
            this.pbx_tharsisi.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_tharsisi.Location = new System.Drawing.Point(226, 350);
            this.pbx_tharsisi.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_tharsisi.Name = "pbx_tharsisi";
            this.pbx_tharsisi.Size = new System.Drawing.Size(14, 15);
            this.pbx_tharsisi.TabIndex = 6;
            this.pbx_tharsisi.TabStop = false;
            this.pbx_tharsisi.Click += new System.EventHandler(this.pbx_tharsisi_Click);
            // 
            // pbx_abfluss
            // 
            this.pbx_abfluss.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbx_abfluss.Location = new System.Drawing.Point(362, 292);
            this.pbx_abfluss.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_abfluss.Name = "pbx_abfluss";
            this.pbx_abfluss.Size = new System.Drawing.Size(16, 15);
            this.pbx_abfluss.TabIndex = 5;
            this.pbx_abfluss.TabStop = false;
            this.pbx_abfluss.Click += new System.EventHandler(this.pbx_abfluss_Click);
            // 
            // pbx_south
            // 
            this.pbx_south.BackColor = System.Drawing.Color.White;
            this.pbx_south.Location = new System.Drawing.Point(463, 511);
            this.pbx_south.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_south.Name = "pbx_south";
            this.pbx_south.Size = new System.Drawing.Size(14, 14);
            this.pbx_south.TabIndex = 3;
            this.pbx_south.TabStop = false;
            this.pbx_south.Click += new System.EventHandler(this.pbx_south_Click);
            // 
            // pbx_north
            // 
            this.pbx_north.BackColor = System.Drawing.Color.White;
            this.pbx_north.Location = new System.Drawing.Point(567, 115);
            this.pbx_north.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_north.Name = "pbx_north";
            this.pbx_north.Size = new System.Drawing.Size(14, 15);
            this.pbx_north.TabIndex = 2;
            this.pbx_north.TabStop = false;
            this.pbx_north.Click += new System.EventHandler(this.pbx_north_Click);
            // 
            // pbx_mars
            // 
            this.pbx_mars.BackgroundImage = global::semesterprojekt_1.Properties.Resources.mars;
            this.pbx_mars.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_mars.Location = new System.Drawing.Point(87, 61);
            this.pbx_mars.Margin = new System.Windows.Forms.Padding(2);
            this.pbx_mars.Name = "pbx_mars";
            this.pbx_mars.Size = new System.Drawing.Size(708, 505);
            this.pbx_mars.TabIndex = 0;
            this.pbx_mars.TabStop = false;
            // 
            // frm_level6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(797, 552);
            this.Controls.Add(this.pb_r);
            this.Controls.Add(this.btn_aufgeben);
            this.Controls.Add(this.lbl_zeit);
            this.Controls.Add(this.pbx_elysium);
            this.Controls.Add(this.pbx_utopia);
            this.Controls.Add(this.pbx_isidis);
            this.Controls.Add(this.pbx_hellas);
            this.Controls.Add(this.pbx_argyre);
            this.Controls.Add(this.pbx_noachis);
            this.Controls.Add(this.pbx_arabia);
            this.Controls.Add(this.pbx_vasitas);
            this.Controls.Add(this.pbx_chryse);
            this.Controls.Add(this.pbx_tempe);
            this.Controls.Add(this.pbx_alba);
            this.Controls.Add(this.pbx_amazonis);
            this.Controls.Add(this.pbx_olympus);
            this.Controls.Add(this.pbx_tharsism);
            this.Controls.Add(this.pbx_valles);
            this.Controls.Add(this.pbx_tharsisi);
            this.Controls.Add(this.pbx_abfluss);
            this.Controls.Add(this.lbl_high);
            this.Controls.Add(this.pbx_south);
            this.Controls.Add(this.pbx_north);
            this.Controls.Add(this.lbl_gebiete);
            this.Controls.Add(this.pbx_mars);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frm_level6";
            this.Text = "Level 6";
            this.Load += new System.EventHandler(this.frm_level6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_elysium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_utopia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_isidis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_hellas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_argyre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_noachis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_arabia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_vasitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_chryse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tempe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_alba)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_amazonis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_olympus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tharsism)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_valles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_tharsisi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_abfluss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_south)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_north)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_mars)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_mars;
        private System.Windows.Forms.Label lbl_gebiete;
        private System.Windows.Forms.PictureBox pbx_north;
        private System.Windows.Forms.PictureBox pbx_south;
        private System.Windows.Forms.Label lbl_high;
        private System.Windows.Forms.PictureBox pbx_abfluss;
        private System.Windows.Forms.PictureBox pbx_tharsisi;
        private System.Windows.Forms.PictureBox pbx_valles;
        private System.Windows.Forms.PictureBox pbx_tharsism;
        private System.Windows.Forms.PictureBox pbx_olympus;
        private System.Windows.Forms.PictureBox pbx_amazonis;
        private System.Windows.Forms.PictureBox pbx_alba;
        private System.Windows.Forms.PictureBox pbx_tempe;
        private System.Windows.Forms.PictureBox pbx_chryse;
        private System.Windows.Forms.PictureBox pbx_vasitas;
        private System.Windows.Forms.PictureBox pbx_arabia;
        private System.Windows.Forms.PictureBox pbx_noachis;
        private System.Windows.Forms.PictureBox pbx_argyre;
        private System.Windows.Forms.PictureBox pbx_hellas;
        private System.Windows.Forms.PictureBox pbx_isidis;
        private System.Windows.Forms.PictureBox pbx_utopia;
        private System.Windows.Forms.PictureBox pbx_elysium;
        private System.Windows.Forms.Timer tmr_lvl6;
        private System.Windows.Forms.Label lbl_zeit;
        private System.Windows.Forms.Button btn_aufgeben;
        private System.Windows.Forms.PictureBox pb_r;
    }
}