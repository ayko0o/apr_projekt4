﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.LinkLabel;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;

namespace semesterprojekt_1
{
    public partial class frm_level2 : Form
    {
        List<string> flaggen = new List<string>();
        Random r = new Random();
        int flagge;
        int hs = 0;
        int c = 0;
        int zeit;
        int level = 2;
        string ben;
        int id;
        int leben;
        bool lvl2;
        string usa = "USA";
        string libanon = "Libanon";
        string kirgisistan = "Kirgisistan";
        string bangladesh = "Bangladesch";
        string chile = "Chile";
        string katar = "Katar";
        string laos = "Laos";
        string tschad = "Tschad";
        string uganda = "Uganda";
        string vatikan = "Vatikanstadt";

        public frm_level2()
        {
            InitializeComponent();
        }
        public frm_level2(string benutzer, int lebenn, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lebenn;
            id = idd;
        }
      

        private void frm_level2_Load_1(object sender, EventArgs e)
        {
            Excel();
            flagge = r.Next(flaggen.Count);
            lbl_land.Text = flaggen[flagge];
            zeit = 120;
            tmr_lvl2.Start();
        }

        private void Excel()
        {
            string file = "C:/lander.xlsx";
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb;
            Worksheet ws;

            wb = excel.Workbooks.Open(file);
            ws = excel.Worksheets[2];


            Range zelle = ws.Range["A1:A15"];

            foreach (string ergebnis in zelle.Value)
            {
                flaggen.Add(ergebnis);
            }
        }

        private void Spiel()
        {
            if (flaggen.Count > 0)
            {
                flagge = r.Next(flaggen.Count);
                lbl_land.Text =  flaggen[flagge];
            }
            if(c==25)
            {
                lvl2 = true;
                tmr_lvl2.Stop();
                hs = zeit + hs;
                cls_dataprovider.Highscore(ben, hs, level, id);
                cls_spiel.Main = lvl2;
                cls_spiel.Leben = leben;
                cls_spiel.Level = level;
                cls_spiel.Highscorelvl2 = hs;
                MessageBox.Show("Du hast die Länder richtig erraten! \nDein Highscore liegt bei: "+ hs +" Punkten.", "Level 2");
                this.Close();
            }
            else if(hs== -10)
            {
                MessageBox.Show("Zu viele Versuche gebraucht. Versuche es erneut.");
                this.Close();
            }

        }

        private void tmr_lvl2_Tick(object sender, EventArgs e)
        {
            lbl_lvl2_zeit.Text = "Zeit:" + zeit--.ToString();

            if (zeit < 1)
            {
                lvl2 = true;
                leben++;
                cls_spiel.Main = lvl2;
                cls_spiel.Leben = leben;
                cls_spiel.Level = level;
                cls_spiel.Highscorelvl2 = hs;
                tmr_lvl2.Stop();
                MessageBox.Show("Du hast es nicht geschafft.", "Level 2");
            }
        }
        private void btn_aufgeben_Click(object sender, EventArgs e)
        {
            lvl2 = true;
            tmr_lvl2.Stop();
            MessageBox.Show("Warum", "Level 2");
            leben++;
            cls_spiel.Leben = leben;
            cls_spiel.Level = level;
            cls_spiel.Main = lvl2;
            cls_spiel.Highscorelvl2 = hs;
            this.Close();
        }

        private void pb_vietnam_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Vietnam")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(usa);
                pb_vietnam.Visible = false;
                pb_usa.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }



        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "China")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(tschad);
                pb_china.Visible = false;
                pb_tschad.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }   
        }

        private void pb_indonesien_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Indonesien")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_indonesien.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

            if (lbl_land.Text == "Indien")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(uganda);
                pb_indien.Visible = false;
                pb_uganda.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_polen_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Polen")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_polen.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_srilanka_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Sri Lanka")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_srilanka.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_liechtenstein_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Liechtenstein")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(libanon);
                pb_liechtenstein.Visible = false;
                pb_libanon.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_pakistan_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Pakistan")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(kirgisistan);
                pb_pakistan.Visible = false;
                pb_kirgisistan.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_nigeria_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Nigeria")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(vatikan);
                pb_nigeria.Visible = false;
                pb_vatikan.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_brasilien_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Brasilien")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(bangladesh);
                pb_brasilien.Visible = false;
                pb_bangladesh.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_usa_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "USA")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_usa.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_luxemburg_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Luxemburg")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(katar);
                pb_luxemburg.Visible = false;
                pb_katar.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_jamaika_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Jamaika")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_jamaika.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_kosovo_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Kosovo")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_kosovo.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_nepal_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Nepal")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(laos);
                pb_nepal.Visible = false;
                pb_laos.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_argentinien_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Argentinien")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                flaggen.Add(chile);
                pb_argentinien.Visible = false;
                pb_chile.Visible = true;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_bangladesh_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Bangladesch")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_bangladesh.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_libanon_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Libanon")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_libanon.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_kirgisistan_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Kirgisistan")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_kirgisistan.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_chile_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Chile")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_chile.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_katar_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Katar")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_katar.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_laos_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Laos")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_laos.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_tschad_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Tschad")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_tschad.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_uganda_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Uganda")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_uganda.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

        private void pb_vatikan_Click(object sender, EventArgs e)
        {
            if (lbl_land.Text == "Vatikanstadt")
            {
                hs++;
                c++;
                lbl_hs.Text = "Highscore: " + hs;
                flaggen.Remove(flaggen[flagge]);
                pb_vatikan.Visible = false;
                Spiel();
            }
            else
            {
                hs--;
                lbl_hs.Text = "Highscore: " + hs;
            }
        }

       
    }
}
