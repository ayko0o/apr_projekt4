﻿namespace semesterprojekt_1
{
    partial class frm_lvl4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_titel = new System.Windows.Forms.Label();
            this.lbl_highscore = new System.Windows.Forms.Label();
            this.btn_a1 = new System.Windows.Forms.Button();
            this.btn_aufgeben = new System.Windows.Forms.Button();
            this.btn_a2 = new System.Windows.Forms.Button();
            this.btn_a3 = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_titel
            // 
            this.lbl_titel.AutoSize = true;
            this.lbl_titel.BackColor = System.Drawing.Color.Thistle;
            this.lbl_titel.Font = new System.Drawing.Font("Myanmar Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_titel.Location = new System.Drawing.Point(20, 27);
            this.lbl_titel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_titel.Name = "lbl_titel";
            this.lbl_titel.Size = new System.Drawing.Size(276, 34);
            this.lbl_titel.TabIndex = 0;
            this.lbl_titel.Text = "Ordne die Nationalhymne zu!";
            // 
            // lbl_highscore
            // 
            this.lbl_highscore.AutoSize = true;
            this.lbl_highscore.BackColor = System.Drawing.Color.Transparent;
            this.lbl_highscore.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_highscore.ForeColor = System.Drawing.Color.Thistle;
            this.lbl_highscore.Location = new System.Drawing.Point(485, 384);
            this.lbl_highscore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_highscore.Name = "lbl_highscore";
            this.lbl_highscore.Size = new System.Drawing.Size(98, 29);
            this.lbl_highscore.TabIndex = 1;
            this.lbl_highscore.Text = "Highscore: ";
            // 
            // btn_a1
            // 
            this.btn_a1.BackColor = System.Drawing.Color.Thistle;
            this.btn_a1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_a1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_a1.Location = new System.Drawing.Point(34, 151);
            this.btn_a1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_a1.Name = "btn_a1";
            this.btn_a1.Size = new System.Drawing.Size(185, 127);
            this.btn_a1.TabIndex = 2;
            this.btn_a1.UseVisualStyleBackColor = false;
            this.btn_a1.Click += new System.EventHandler(this.btn_a1_Click);
            // 
            // btn_aufgeben
            // 
            this.btn_aufgeben.BackColor = System.Drawing.Color.Thistle;
            this.btn_aufgeben.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_aufgeben.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_aufgeben.Location = new System.Drawing.Point(244, 316);
            this.btn_aufgeben.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_aufgeben.Name = "btn_aufgeben";
            this.btn_aufgeben.Size = new System.Drawing.Size(230, 47);
            this.btn_aufgeben.TabIndex = 5;
            this.btn_aufgeben.Text = "Aufgeben";
            this.btn_aufgeben.UseVisualStyleBackColor = false;
            this.btn_aufgeben.Click += new System.EventHandler(this.btn_aufgeben_Click);
            // 
            // btn_a2
            // 
            this.btn_a2.BackColor = System.Drawing.Color.Thistle;
            this.btn_a2.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_a2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_a2.Location = new System.Drawing.Point(262, 151);
            this.btn_a2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_a2.Name = "btn_a2";
            this.btn_a2.Size = new System.Drawing.Size(185, 127);
            this.btn_a2.TabIndex = 2;
            this.btn_a2.UseVisualStyleBackColor = false;
            this.btn_a2.Click += new System.EventHandler(this.btn_a2_Click);
            // 
            // btn_a3
            // 
            this.btn_a3.BackColor = System.Drawing.Color.Thistle;
            this.btn_a3.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_a3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_a3.Location = new System.Drawing.Point(492, 151);
            this.btn_a3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_a3.Name = "btn_a3";
            this.btn_a3.Size = new System.Drawing.Size(185, 127);
            this.btn_a3.TabIndex = 2;
            this.btn_a3.UseVisualStyleBackColor = false;
            this.btn_a3.Click += new System.EventHandler(this.btn_a3_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::semesterprojekt_1.Properties.Resources.froggy1;
            this.pictureBox6.Location = new System.Drawing.Point(322, 41);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(59, 44);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 34;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::semesterprojekt_1.Properties.Resources.zucker11;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Location = new System.Drawing.Point(626, 350);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(74, 72);
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = global::semesterprojekt_1.Properties.Resources.cloud2;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Location = new System.Drawing.Point(409, -47);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(355, 292);
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::semesterprojekt_1.Properties.Resources.cloud1;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Location = new System.Drawing.Point(-31, -39);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(478, 268);
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::semesterprojekt_1.Properties.Resources.zucker1;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(-13, 258);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(180, 178);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // frm_lvl4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(698, 418);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lbl_titel);
            this.Controls.Add(this.btn_aufgeben);
            this.Controls.Add(this.btn_a3);
            this.Controls.Add(this.btn_a2);
            this.Controls.Add(this.btn_a1);
            this.Controls.Add(this.lbl_highscore);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frm_lvl4";
            this.Text = "Level 4";
            this.Load += new System.EventHandler(this.frm_lvl4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_titel;
        private System.Windows.Forms.Label lbl_highscore;
        private System.Windows.Forms.Button btn_a1;
        private System.Windows.Forms.Button btn_aufgeben;
        private System.Windows.Forms.Button btn_a2;
        private System.Windows.Forms.Button btn_a3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}