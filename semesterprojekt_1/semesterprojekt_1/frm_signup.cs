using System;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_signup : Form
    {
        public frm_signup()
        {
            InitializeComponent();
        }

        private void frm_signup_Load(object sender, EventArgs e)
        {

        }

        private void btn_schliesen_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_loschen_Click(object sender, EventArgs e)
        {
            tbx_vorname.Text = "";
            tbx_nachname.Text = "";
            tbx_benutzer.Text = "";
            tbx_passwort.Text = "";
            dtp_geburtsdatum.Text = "";
            tbx_familienstand.Text = "";
            tbx_haarfarbe.Text = "";
            tbx_augenfarbe.Text = "";
            tbx_blutgruppe.Text = "";
            tbx_email.Text = "";
            tbx_telefonnummer.Text = "";
            tbx_ort.Text = "";
            tbx_postleitzahl.Text = "";
            tbx_adresse.Text = "";
            rbtn_ja.Checked = false;
            rbtn_nein.Checked = false;
        }

        private void btn_registrieren_Click(object sender, EventArgs e)
        {
            if (rbtn_ja.Checked)
            {
                cls_konto konto = new cls_konto(tbx_vorname.Text, tbx_nachname.Text, tbx_benutzer.Text, tbx_passwort.Text, Convert.ToDateTime(dtp_geburtsdatum.Value), tbx_adresse.Text, tbx_ort.Text, tbx_postleitzahl.Text, tbx_familienstand.Text, tbx_telefonnummer.Text, tbx_email.Text, tbx_blutgruppe.Text, tbx_haarfarbe.Text, tbx_augenfarbe.Text, rbtn_ja.Text);
                cls_dataprovider.SignUp(konto);
                DialogResult = DialogResult.OK;
            }
            else if (rbtn_nein.Checked)
            {
                cls_konto konto = new cls_konto(tbx_vorname.Text, tbx_nachname.Text, tbx_benutzer.Text, tbx_passwort.Text, Convert.ToDateTime(dtp_geburtsdatum.Value), tbx_adresse.Text, tbx_ort.Text, tbx_postleitzahl.Text, tbx_familienstand.Text, tbx_telefonnummer.Text, tbx_email.Text, tbx_blutgruppe.Text, tbx_haarfarbe.Text, tbx_augenfarbe.Text, rbtn_nein.Text);
                cls_dataprovider.SignUp(konto);
                DialogResult = DialogResult.OK;
            }
            else
            {
                cls_konto konto = new cls_konto(tbx_vorname.Text, tbx_nachname.Text, tbx_benutzer.Text, tbx_passwort.Text, Convert.ToDateTime(dtp_geburtsdatum.Value), tbx_adresse.Text, tbx_ort.Text, tbx_postleitzahl.Text, tbx_familienstand.Text, tbx_telefonnummer.Text, tbx_email.Text, tbx_blutgruppe.Text, tbx_haarfarbe.Text, tbx_augenfarbe.Text, "");
                cls_dataprovider.SignUp(konto);
                DialogResult = DialogResult.OK;
            }
        }
    }
}
