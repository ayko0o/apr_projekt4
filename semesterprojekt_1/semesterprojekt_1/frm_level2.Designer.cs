﻿namespace semesterprojekt_1
{
    partial class frm_level2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmr_lvl2 = new System.Windows.Forms.Timer(this.components);
            this.lbl_land = new System.Windows.Forms.Label();
            this.lbl_hs = new System.Windows.Forms.Label();
            this.lbl_lvl2_zeit = new System.Windows.Forms.Label();
            this.btn_aufgeben = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pb_vatikan = new System.Windows.Forms.PictureBox();
            this.pb_uganda = new System.Windows.Forms.PictureBox();
            this.pb_tschad = new System.Windows.Forms.PictureBox();
            this.pb_laos = new System.Windows.Forms.PictureBox();
            this.pb_katar = new System.Windows.Forms.PictureBox();
            this.pb_chile = new System.Windows.Forms.PictureBox();
            this.pb_bangladesh = new System.Windows.Forms.PictureBox();
            this.pb_libanon = new System.Windows.Forms.PictureBox();
            this.pb_kirgisistan = new System.Windows.Forms.PictureBox();
            this.pb_usa = new System.Windows.Forms.PictureBox();
            this.pb_argentinien = new System.Windows.Forms.PictureBox();
            this.pb_nepal = new System.Windows.Forms.PictureBox();
            this.pb_kosovo = new System.Windows.Forms.PictureBox();
            this.pb_jamaika = new System.Windows.Forms.PictureBox();
            this.pb_luxemburg = new System.Windows.Forms.PictureBox();
            this.pb_brasilien = new System.Windows.Forms.PictureBox();
            this.pb_nigeria = new System.Windows.Forms.PictureBox();
            this.pb_pakistan = new System.Windows.Forms.PictureBox();
            this.pb_china = new System.Windows.Forms.PictureBox();
            this.pb_indien = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pb_liechtenstein = new System.Windows.Forms.PictureBox();
            this.pb_vietnam = new System.Windows.Forms.PictureBox();
            this.pb_polen = new System.Windows.Forms.PictureBox();
            this.pb_srilanka = new System.Windows.Forms.PictureBox();
            this.pb_indonesien = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_vatikan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_uganda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_tschad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_laos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_katar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_bangladesh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_libanon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kirgisistan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_usa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_argentinien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nepal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kosovo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_jamaika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_luxemburg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_brasilien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nigeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pakistan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_china)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_indien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_liechtenstein)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_vietnam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_polen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_srilanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_indonesien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tmr_lvl2
            // 
            this.tmr_lvl2.Enabled = true;
            this.tmr_lvl2.Interval = 1000;
            this.tmr_lvl2.Tick += new System.EventHandler(this.tmr_lvl2_Tick);
            // 
            // lbl_land
            // 
            this.lbl_land.AutoSize = true;
            this.lbl_land.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.lbl_land.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_land.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_land.Location = new System.Drawing.Point(89, 72);
            this.lbl_land.Name = "lbl_land";
            this.lbl_land.Size = new System.Drawing.Size(26, 29);
            this.lbl_land.TabIndex = 12;
            this.lbl_land.Text = "1";
            // 
            // lbl_hs
            // 
            this.lbl_hs.AutoSize = true;
            this.lbl_hs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(185)))), ((int)(((byte)(183)))));
            this.lbl_hs.Font = new System.Drawing.Font("MingLiU-ExtB", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.lbl_hs.Location = new System.Drawing.Point(300, 94);
            this.lbl_hs.Name = "lbl_hs";
            this.lbl_hs.Size = new System.Drawing.Size(140, 24);
            this.lbl_hs.TabIndex = 19;
            this.lbl_hs.Text = "Highscore:";
            // 
            // lbl_lvl2_zeit
            // 
            this.lbl_lvl2_zeit.AutoSize = true;
            this.lbl_lvl2_zeit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(185)))), ((int)(((byte)(183)))));
            this.lbl_lvl2_zeit.Font = new System.Drawing.Font("MingLiU-ExtB", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lvl2_zeit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.lbl_lvl2_zeit.Location = new System.Drawing.Point(300, 43);
            this.lbl_lvl2_zeit.Name = "lbl_lvl2_zeit";
            this.lbl_lvl2_zeit.Size = new System.Drawing.Size(75, 24);
            this.lbl_lvl2_zeit.TabIndex = 35;
            this.lbl_lvl2_zeit.Text = "Zeit:";
            // 
            // btn_aufgeben
            // 
            this.btn_aufgeben.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.btn_aufgeben.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_aufgeben.Font = new System.Drawing.Font("MingLiU-ExtB", 12F, System.Drawing.FontStyle.Bold);
            this.btn_aufgeben.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_aufgeben.Location = new System.Drawing.Point(547, 90);
            this.btn_aufgeben.Name = "btn_aufgeben";
            this.btn_aufgeben.Size = new System.Drawing.Size(122, 43);
            this.btn_aufgeben.TabIndex = 36;
            this.btn_aufgeben.Text = "Aufgeben";
            this.btn_aufgeben.UseVisualStyleBackColor = false;
            this.btn_aufgeben.Click += new System.EventHandler(this.btn_aufgeben_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::semesterprojekt_1.Properties.Resources.frog4;
            this.pictureBox3.Location = new System.Drawing.Point(80, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(82, 58);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 37;
            this.pictureBox3.TabStop = false;
            // 
            // pb_vatikan
            // 
            this.pb_vatikan.Image = global::semesterprojekt_1.Properties.Resources.vatikan;
            this.pb_vatikan.Location = new System.Drawing.Point(450, 271);
            this.pb_vatikan.Name = "pb_vatikan";
            this.pb_vatikan.Size = new System.Drawing.Size(71, 45);
            this.pb_vatikan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_vatikan.TabIndex = 34;
            this.pb_vatikan.TabStop = false;
            this.pb_vatikan.Tag = "flagge";
            this.pb_vatikan.Visible = false;
            this.pb_vatikan.Click += new System.EventHandler(this.pb_vatikan_Click);
            // 
            // pb_uganda
            // 
            this.pb_uganda.Image = global::semesterprojekt_1.Properties.Resources.uganda;
            this.pb_uganda.Location = new System.Drawing.Point(108, 271);
            this.pb_uganda.Name = "pb_uganda";
            this.pb_uganda.Size = new System.Drawing.Size(71, 45);
            this.pb_uganda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_uganda.TabIndex = 33;
            this.pb_uganda.TabStop = false;
            this.pb_uganda.Tag = "flagge";
            this.pb_uganda.Visible = false;
            this.pb_uganda.Click += new System.EventHandler(this.pb_uganda_Click);
            // 
            // pb_tschad
            // 
            this.pb_tschad.Image = global::semesterprojekt_1.Properties.Resources.tschad;
            this.pb_tschad.Location = new System.Drawing.Point(222, 271);
            this.pb_tschad.Name = "pb_tschad";
            this.pb_tschad.Size = new System.Drawing.Size(71, 45);
            this.pb_tschad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_tschad.TabIndex = 32;
            this.pb_tschad.TabStop = false;
            this.pb_tschad.Tag = "flagge";
            this.pb_tschad.Visible = false;
            this.pb_tschad.Click += new System.EventHandler(this.pb_tschad_Click);
            // 
            // pb_laos
            // 
            this.pb_laos.Image = global::semesterprojekt_1.Properties.Resources.laos;
            this.pb_laos.Location = new System.Drawing.Point(450, 355);
            this.pb_laos.Name = "pb_laos";
            this.pb_laos.Size = new System.Drawing.Size(71, 45);
            this.pb_laos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_laos.TabIndex = 31;
            this.pb_laos.TabStop = false;
            this.pb_laos.Tag = "flagge";
            this.pb_laos.Visible = false;
            this.pb_laos.Click += new System.EventHandler(this.pb_laos_Click);
            // 
            // pb_katar
            // 
            this.pb_katar.Image = global::semesterprojekt_1.Properties.Resources.katar;
            this.pb_katar.Location = new System.Drawing.Point(108, 355);
            this.pb_katar.Name = "pb_katar";
            this.pb_katar.Size = new System.Drawing.Size(71, 45);
            this.pb_katar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_katar.TabIndex = 30;
            this.pb_katar.TabStop = false;
            this.pb_katar.Tag = "flagge";
            this.pb_katar.Visible = false;
            this.pb_katar.Click += new System.EventHandler(this.pb_katar_Click);
            // 
            // pb_chile
            // 
            this.pb_chile.Image = global::semesterprojekt_1.Properties.Resources.chile;
            this.pb_chile.Location = new System.Drawing.Point(563, 355);
            this.pb_chile.Name = "pb_chile";
            this.pb_chile.Size = new System.Drawing.Size(71, 45);
            this.pb_chile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_chile.TabIndex = 29;
            this.pb_chile.TabStop = false;
            this.pb_chile.Tag = "flagge";
            this.pb_chile.Visible = false;
            this.pb_chile.Click += new System.EventHandler(this.pb_chile_Click);
            // 
            // pb_bangladesh
            // 
            this.pb_bangladesh.Image = global::semesterprojekt_1.Properties.Resources.bangladesh;
            this.pb_bangladesh.Location = new System.Drawing.Point(563, 271);
            this.pb_bangladesh.Name = "pb_bangladesh";
            this.pb_bangladesh.Size = new System.Drawing.Size(71, 45);
            this.pb_bangladesh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_bangladesh.TabIndex = 28;
            this.pb_bangladesh.TabStop = false;
            this.pb_bangladesh.Tag = "flagge";
            this.pb_bangladesh.Visible = false;
            this.pb_bangladesh.Click += new System.EventHandler(this.pb_bangladesh_Click);
            // 
            // pb_libanon
            // 
            this.pb_libanon.Image = global::semesterprojekt_1.Properties.Resources.libanon;
            this.pb_libanon.Location = new System.Drawing.Point(450, 181);
            this.pb_libanon.Name = "pb_libanon";
            this.pb_libanon.Size = new System.Drawing.Size(71, 45);
            this.pb_libanon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_libanon.TabIndex = 27;
            this.pb_libanon.TabStop = false;
            this.pb_libanon.Tag = "flagge";
            this.pb_libanon.Visible = false;
            this.pb_libanon.Click += new System.EventHandler(this.pb_libanon_Click);
            // 
            // pb_kirgisistan
            // 
            this.pb_kirgisistan.Image = global::semesterprojekt_1.Properties.Resources.kirgisistan;
            this.pb_kirgisistan.Location = new System.Drawing.Point(339, 271);
            this.pb_kirgisistan.Name = "pb_kirgisistan";
            this.pb_kirgisistan.Size = new System.Drawing.Size(71, 45);
            this.pb_kirgisistan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_kirgisistan.TabIndex = 26;
            this.pb_kirgisistan.TabStop = false;
            this.pb_kirgisistan.Tag = "flagge";
            this.pb_kirgisistan.Visible = false;
            this.pb_kirgisistan.Click += new System.EventHandler(this.pb_kirgisistan_Click);
            // 
            // pb_usa
            // 
            this.pb_usa.Image = global::semesterprojekt_1.Properties.Resources._3_usa;
            this.pb_usa.Location = new System.Drawing.Point(108, 181);
            this.pb_usa.Name = "pb_usa";
            this.pb_usa.Size = new System.Drawing.Size(71, 45);
            this.pb_usa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_usa.TabIndex = 25;
            this.pb_usa.TabStop = false;
            this.pb_usa.Tag = "flagge";
            this.pb_usa.Visible = false;
            this.pb_usa.Click += new System.EventHandler(this.pb_usa_Click);
            // 
            // pb_argentinien
            // 
            this.pb_argentinien.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pb_argentinien.Image = global::semesterprojekt_1.Properties.Resources.argentinien;
            this.pb_argentinien.Location = new System.Drawing.Point(563, 355);
            this.pb_argentinien.Name = "pb_argentinien";
            this.pb_argentinien.Size = new System.Drawing.Size(71, 45);
            this.pb_argentinien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_argentinien.TabIndex = 24;
            this.pb_argentinien.TabStop = false;
            this.pb_argentinien.Tag = "flagge";
            this.pb_argentinien.Click += new System.EventHandler(this.pb_argentinien_Click);
            // 
            // pb_nepal
            // 
            this.pb_nepal.Image = global::semesterprojekt_1.Properties.Resources.nepal1;
            this.pb_nepal.Location = new System.Drawing.Point(450, 355);
            this.pb_nepal.Name = "pb_nepal";
            this.pb_nepal.Size = new System.Drawing.Size(71, 45);
            this.pb_nepal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_nepal.TabIndex = 23;
            this.pb_nepal.TabStop = false;
            this.pb_nepal.Tag = "flagge";
            this.pb_nepal.Click += new System.EventHandler(this.pb_nepal_Click);
            // 
            // pb_kosovo
            // 
            this.pb_kosovo.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pb_kosovo.Image = global::semesterprojekt_1.Properties.Resources.kosovo;
            this.pb_kosovo.Location = new System.Drawing.Point(339, 355);
            this.pb_kosovo.Name = "pb_kosovo";
            this.pb_kosovo.Size = new System.Drawing.Size(71, 45);
            this.pb_kosovo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_kosovo.TabIndex = 22;
            this.pb_kosovo.TabStop = false;
            this.pb_kosovo.Tag = "flagge";
            this.pb_kosovo.Click += new System.EventHandler(this.pb_kosovo_Click);
            // 
            // pb_jamaika
            // 
            this.pb_jamaika.Image = global::semesterprojekt_1.Properties.Resources.jamaika;
            this.pb_jamaika.Location = new System.Drawing.Point(222, 355);
            this.pb_jamaika.Name = "pb_jamaika";
            this.pb_jamaika.Size = new System.Drawing.Size(71, 45);
            this.pb_jamaika.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_jamaika.TabIndex = 21;
            this.pb_jamaika.TabStop = false;
            this.pb_jamaika.Tag = "flagge";
            this.pb_jamaika.Click += new System.EventHandler(this.pb_jamaika_Click);
            // 
            // pb_luxemburg
            // 
            this.pb_luxemburg.Image = global::semesterprojekt_1.Properties.Resources.luxemburg;
            this.pb_luxemburg.Location = new System.Drawing.Point(108, 355);
            this.pb_luxemburg.Name = "pb_luxemburg";
            this.pb_luxemburg.Size = new System.Drawing.Size(71, 45);
            this.pb_luxemburg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_luxemburg.TabIndex = 20;
            this.pb_luxemburg.TabStop = false;
            this.pb_luxemburg.Tag = "flagge";
            this.pb_luxemburg.Click += new System.EventHandler(this.pb_luxemburg_Click);
            // 
            // pb_brasilien
            // 
            this.pb_brasilien.Image = global::semesterprojekt_1.Properties.Resources._7_brasilien;
            this.pb_brasilien.Location = new System.Drawing.Point(563, 271);
            this.pb_brasilien.Name = "pb_brasilien";
            this.pb_brasilien.Size = new System.Drawing.Size(71, 45);
            this.pb_brasilien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_brasilien.TabIndex = 18;
            this.pb_brasilien.TabStop = false;
            this.pb_brasilien.Tag = "flagge";
            this.pb_brasilien.Click += new System.EventHandler(this.pb_brasilien_Click);
            // 
            // pb_nigeria
            // 
            this.pb_nigeria.Image = global::semesterprojekt_1.Properties.Resources._6_nigeria;
            this.pb_nigeria.Location = new System.Drawing.Point(450, 271);
            this.pb_nigeria.Name = "pb_nigeria";
            this.pb_nigeria.Size = new System.Drawing.Size(71, 45);
            this.pb_nigeria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_nigeria.TabIndex = 17;
            this.pb_nigeria.TabStop = false;
            this.pb_nigeria.Tag = "flagge";
            this.pb_nigeria.Click += new System.EventHandler(this.pb_nigeria_Click);
            // 
            // pb_pakistan
            // 
            this.pb_pakistan.Image = global::semesterprojekt_1.Properties.Resources._5_pakistan;
            this.pb_pakistan.Location = new System.Drawing.Point(339, 271);
            this.pb_pakistan.Name = "pb_pakistan";
            this.pb_pakistan.Size = new System.Drawing.Size(71, 45);
            this.pb_pakistan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_pakistan.TabIndex = 16;
            this.pb_pakistan.TabStop = false;
            this.pb_pakistan.Tag = "flagge";
            this.pb_pakistan.Click += new System.EventHandler(this.pb_pakistan_Click);
            // 
            // pb_china
            // 
            this.pb_china.Image = global::semesterprojekt_1.Properties.Resources._1_china;
            this.pb_china.Location = new System.Drawing.Point(222, 271);
            this.pb_china.Name = "pb_china";
            this.pb_china.Size = new System.Drawing.Size(71, 45);
            this.pb_china.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_china.TabIndex = 15;
            this.pb_china.TabStop = false;
            this.pb_china.Tag = "flagge";
            this.pb_china.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pb_indien
            // 
            this.pb_indien.Image = global::semesterprojekt_1.Properties.Resources._2_indien;
            this.pb_indien.Location = new System.Drawing.Point(108, 271);
            this.pb_indien.Name = "pb_indien";
            this.pb_indien.Size = new System.Drawing.Size(71, 45);
            this.pb_indien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_indien.TabIndex = 14;
            this.pb_indien.TabStop = false;
            this.pb_indien.Tag = "flagge";
            this.pb_indien.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.pictureBox1.Location = new System.Drawing.Point(64, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(216, 66);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pb_liechtenstein
            // 
            this.pb_liechtenstein.Image = global::semesterprojekt_1.Properties.Resources.liechtenstein;
            this.pb_liechtenstein.Location = new System.Drawing.Point(450, 181);
            this.pb_liechtenstein.Name = "pb_liechtenstein";
            this.pb_liechtenstein.Size = new System.Drawing.Size(71, 45);
            this.pb_liechtenstein.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_liechtenstein.TabIndex = 9;
            this.pb_liechtenstein.TabStop = false;
            this.pb_liechtenstein.Tag = "flagge";
            this.pb_liechtenstein.Click += new System.EventHandler(this.pb_liechtenstein_Click);
            // 
            // pb_vietnam
            // 
            this.pb_vietnam.Image = global::semesterprojekt_1.Properties.Resources.vietnam;
            this.pb_vietnam.Location = new System.Drawing.Point(108, 181);
            this.pb_vietnam.Name = "pb_vietnam";
            this.pb_vietnam.Size = new System.Drawing.Size(71, 45);
            this.pb_vietnam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_vietnam.TabIndex = 8;
            this.pb_vietnam.TabStop = false;
            this.pb_vietnam.Tag = "flagge";
            this.pb_vietnam.Click += new System.EventHandler(this.pb_vietnam_Click);
            // 
            // pb_polen
            // 
            this.pb_polen.Image = global::semesterprojekt_1.Properties.Resources.polen;
            this.pb_polen.Location = new System.Drawing.Point(222, 181);
            this.pb_polen.Name = "pb_polen";
            this.pb_polen.Size = new System.Drawing.Size(71, 45);
            this.pb_polen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_polen.TabIndex = 7;
            this.pb_polen.TabStop = false;
            this.pb_polen.Tag = "flagge";
            this.pb_polen.Click += new System.EventHandler(this.pb_polen_Click);
            // 
            // pb_srilanka
            // 
            this.pb_srilanka.Image = global::semesterprojekt_1.Properties.Resources.sri_lanka;
            this.pb_srilanka.Location = new System.Drawing.Point(339, 181);
            this.pb_srilanka.Name = "pb_srilanka";
            this.pb_srilanka.Size = new System.Drawing.Size(71, 45);
            this.pb_srilanka.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_srilanka.TabIndex = 6;
            this.pb_srilanka.TabStop = false;
            this.pb_srilanka.Tag = "flagge";
            this.pb_srilanka.Click += new System.EventHandler(this.pb_srilanka_Click);
            // 
            // pb_indonesien
            // 
            this.pb_indonesien.Image = global::semesterprojekt_1.Properties.Resources._4_indonesien;
            this.pb_indonesien.Location = new System.Drawing.Point(563, 181);
            this.pb_indonesien.Name = "pb_indonesien";
            this.pb_indonesien.Size = new System.Drawing.Size(71, 45);
            this.pb_indonesien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_indonesien.TabIndex = 3;
            this.pb_indonesien.TabStop = false;
            this.pb_indonesien.Tag = "flagge";
            this.pb_indonesien.Click += new System.EventHandler(this.pb_indonesien_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(116)))), ((int)(((byte)(122)))));
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(64, 139);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(605, 309);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // frm_level2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(185)))), ((int)(((byte)(183)))));
            this.ClientSize = new System.Drawing.Size(735, 511);
            this.Controls.Add(this.btn_aufgeben);
            this.Controls.Add(this.lbl_lvl2_zeit);
            this.Controls.Add(this.pb_vatikan);
            this.Controls.Add(this.pb_uganda);
            this.Controls.Add(this.pb_tschad);
            this.Controls.Add(this.pb_laos);
            this.Controls.Add(this.pb_katar);
            this.Controls.Add(this.pb_chile);
            this.Controls.Add(this.pb_bangladesh);
            this.Controls.Add(this.pb_libanon);
            this.Controls.Add(this.pb_kirgisistan);
            this.Controls.Add(this.pb_usa);
            this.Controls.Add(this.pb_argentinien);
            this.Controls.Add(this.pb_nepal);
            this.Controls.Add(this.pb_kosovo);
            this.Controls.Add(this.pb_jamaika);
            this.Controls.Add(this.pb_luxemburg);
            this.Controls.Add(this.lbl_hs);
            this.Controls.Add(this.pb_brasilien);
            this.Controls.Add(this.pb_nigeria);
            this.Controls.Add(this.pb_pakistan);
            this.Controls.Add(this.pb_china);
            this.Controls.Add(this.pb_indien);
            this.Controls.Add(this.lbl_land);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pb_liechtenstein);
            this.Controls.Add(this.pb_vietnam);
            this.Controls.Add(this.pb_polen);
            this.Controls.Add(this.pb_srilanka);
            this.Controls.Add(this.pb_indonesien);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frm_level2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Level 2";
            this.Load += new System.EventHandler(this.frm_level2_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_vatikan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_uganda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_tschad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_laos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_katar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_bangladesh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_libanon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kirgisistan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_usa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_argentinien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nepal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kosovo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_jamaika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_luxemburg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_brasilien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nigeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pakistan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_china)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_indien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_liechtenstein)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_vietnam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_polen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_srilanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_indonesien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pb_indonesien;
        private System.Windows.Forms.PictureBox pb_srilanka;
        private System.Windows.Forms.PictureBox pb_polen;
        private System.Windows.Forms.PictureBox pb_vietnam;
        private System.Windows.Forms.PictureBox pb_liechtenstein;
        private System.Windows.Forms.Timer tmr_lvl2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_land;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pb_indien;
        private System.Windows.Forms.PictureBox pb_china;
        private System.Windows.Forms.PictureBox pb_pakistan;
        private System.Windows.Forms.PictureBox pb_nigeria;
        private System.Windows.Forms.PictureBox pb_brasilien;
        private System.Windows.Forms.Label lbl_hs;
        private System.Windows.Forms.PictureBox pb_luxemburg;
        private System.Windows.Forms.PictureBox pb_jamaika;
        private System.Windows.Forms.PictureBox pb_kosovo;
        private System.Windows.Forms.PictureBox pb_nepal;
        private System.Windows.Forms.PictureBox pb_argentinien;
        private System.Windows.Forms.PictureBox pb_usa;
        private System.Windows.Forms.PictureBox pb_kirgisistan;
        private System.Windows.Forms.PictureBox pb_libanon;
        private System.Windows.Forms.PictureBox pb_bangladesh;
        private System.Windows.Forms.PictureBox pb_chile;
        private System.Windows.Forms.PictureBox pb_katar;
        private System.Windows.Forms.PictureBox pb_laos;
        private System.Windows.Forms.PictureBox pb_tschad;
        private System.Windows.Forms.PictureBox pb_uganda;
        private System.Windows.Forms.PictureBox pb_vatikan;
        private System.Windows.Forms.Label lbl_lvl2_zeit;
        private System.Windows.Forms.Button btn_aufgeben;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}