﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_level3 : Form
    {
        bool links, rechts = false;
        int hs;
        int count;
        int level = 3;
        string ben;
        int id;
        int leben;
        bool lvl3;
        bool b = false;
        bool a = false;
        bool g = false;
        bool k = false;
        bool m = false;
        
        public frm_level3()
        {
            InitializeComponent();
        }
        public frm_level3(string benutzer, int lebenn, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lebenn;
            id = idd;
        }
        private void frm_level3_Load(object sender, EventArgs e)
        {

        }
        private void frm_level3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                rechts = true;
            }
            if (e.KeyCode == Keys.Left)
            {
                links = true;
            }

        }
        private void frm_level3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                rechts = false;
            }
            if (e.KeyCode == Keys.Left)
            {
                links = false;
            }

        }
        
        private void Char_Bewegung()
        {
            if (rechts == true)
            {
                if (pb_r.Left < 550)
                {
                    pb_r.Left += 10;
                }
            }
            if (links == true)
            {

                if (pb_r.Left > 10)
                {
                    pb_r.Left -= 10;
                }
            }

        }

        private void Spiel()
        {
          
            if(pb_r.Bounds.IntersectsWith(pb_burundi.Bounds))
            {
                count++;
                hs =hs + 50;
                lbl_hs.Text = "" + hs;
                lbl_count.Text = count+"/5";
                b = true;

            }
            else if (pb_r.Bounds.IntersectsWith(pb_aegypten.Bounds))
            {
                count++;
                hs = hs + 50;
                lbl_hs.Text = "" + hs;
                lbl_count.Text = count + "/5";
                a = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_ghana.Bounds))
            {
                count++;
                hs = hs + 50;
                lbl_hs.Text = "" + hs;
                lbl_count.Text = count + "/5";
                g = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_kapverde.Bounds))
            {
                count++;
                hs = hs + 50;
                lbl_hs.Text = "" + hs;
                lbl_count.Text = count + "/5";
                k = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_marokko.Bounds))
            {
                count++;
                hs = hs + 50;
                lbl_hs.Text = "" + hs;
                lbl_count.Text = count + "/5";
                m = true;
            }


            if (pb_r.Bounds.IntersectsWith(pb_laos.Bounds))
            {
                hs = hs - 1;
                lbl_hs.Text = "" + hs;
                pb_laos.Visible = false;
                lbl_laos.Visible = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_nordkorea.Bounds))
            {
                hs = hs - 1;
                lbl_hs.Text = "" + hs;
                pb_nordkorea.Visible = false;
                lbl_nordkorea.Visible = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_jordanien.Bounds))
            {
                hs = hs - 1;
                lbl_hs.Text = "" + hs;
                pb_jordanien.Visible = false;
                lbl_jordanien.Visible = true;
            }
            else if (pb_r.Bounds.IntersectsWith(pb_bangladesh.Bounds))
            {
                hs = hs - 1;
                lbl_hs.Text = "" + hs;
                pb_bangladesh.Visible = false;
                lbl_bangladesh.Visible = true;
            }

            if (b == true)
            {
                pb_burundi.Top = 1000;
            }
            if(a == true)
            {
                pb_aegypten.Top = 1000;
            }
            if(g== true) 
            {
                pb_ghana.Top = 1000;
            }
            if(k== true)
            {
                pb_kapverde.Top = 1000;
            }
            if(m== true)
            {
                pb_marokko.Top = 1000;
            }

            if(count == 5)
            {
                lvl3 = true;
                tmr_lvl3.Stop();
                cls_dataprovider.Highscore(ben, hs, level, id);
                cls_spiel.Main = lvl3;
                cls_spiel.Leben = leben;
                cls_spiel.Highscore = hs;
                cls_spiel.Level = level;
                cls_spiel.Highscorelvl3 = hs;
                MessageBox.Show("Du hast es geschafft \nDein Highscore liegt bei " + hs+ " Punkten!", "Level 3");
                this.Close();
            }
            else if(hs == -100)
            {
                tmr_lvl3.Stop();
                lvl3 = true;
                leben++;
                cls_spiel.Main = lvl3;
                cls_spiel.Leben = leben;
                cls_spiel.Highscore = hs;
                cls_spiel.Level = level;
                MessageBox.Show("Nein", "Level 3 nicht geschafft");
                this.Close();
            }


        }
        private void Flaggen_Bew()
        {
            Random r = new Random();
            int x, y;
            if (pb_burundi.Top >= 700)
            {
                x = r.Next(0, 300);
                pb_burundi.Location = new Point(x, 0);
            }
            if (pb_aegypten.Top >= 1000)
            {
                x = r.Next(0, 300);
                pb_aegypten.Location = new Point(x, 0);
            }
            if (pb_ghana.Top >= 900)
            {
                x = r.Next(0, 500);
                pb_ghana.Location = new Point(x, 0);
            }
            if (pb_kapverde.Top >= 1200)
            {
                x = r.Next(0, 300);
                pb_kapverde.Location = new Point(x, 0);
            }
            if (pb_marokko.Top >= 1800)
            {
                x = r.Next(0, 500);
                pb_marokko.Location = new Point(x, 0);
            }


            if (pb_laos.Top >= 700)
            {
                y = r.Next(0, 300);
                pb_laos.Location = new Point(y, 0);
                pb_laos.Visible = true;
            }
            if (pb_nordkorea.Top >= 900)
            {
                y = r.Next(0, 300);
                pb_nordkorea.Location = new Point(y, 0);
                pb_nordkorea.Visible = true;
            }
            if (pb_jordanien.Top >= 1400)
            {
                y = r.Next(0, 300);
                pb_jordanien.Location = new Point(y, 0);
                pb_jordanien.Visible = true;
            }
            if (pb_bangladesh.Top >= 1800)
            {
                y = r.Next(0, 300);
                pb_bangladesh.Location = new Point(y, 0);
                pb_bangladesh.Visible = true;
            }

            else
            {
                pb_burundi.Top += 3;
                pb_aegypten.Top += 4;
                pb_ghana.Top += 3;
                pb_kapverde.Top += 4;
                pb_marokko.Top += 4;

                pb_laos.Top += 3;
                pb_nordkorea.Top += 4;
                pb_jordanien.Top += 4;
                pb_bangladesh.Top += 5;

            }
        }

 

        private void tmr_lvl3_Tick(object sender, EventArgs e)
        {
            Char_Bewegung();
            Flaggen_Bew();
            Spiel();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }


      
    }
}
