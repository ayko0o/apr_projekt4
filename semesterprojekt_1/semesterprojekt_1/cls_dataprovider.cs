﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Reflection.Emit;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    internal class cls_dataprovider
    {
        const string connstring = "datasource=127.0.0.1;port=3306;username=root;password=;database=db_semesterprojekt;";

        public static void SignUp(cls_konto konto)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query = "INSERT INTO tbl_signup (vorname, nachname, benutzer, passwort, geburtsdatum, adresse, ort, postleitzahl, familienstatus, telefonnummer, email, blutgruppe, haarfarbe, augenfarbe, brille) VALUES (@vorname, @nachname, @benutzer, @passwort, @geburtsdatum, @adresse, @ort, @postleitzahl, @familienstatus, @telefonnummer, @email, @blutgruppe, @haarfarbe, @augenfarbe, @brille);";
            MySqlCommand cmd = new MySqlCommand(query, dc);

            cmd.Parameters.Add("@vorname", MySqlDbType.VarChar).Value = konto.Vorname;
            cmd.Parameters.Add("@nachname", MySqlDbType.VarChar).Value = konto.Nachname;
            cmd.Parameters.Add("@benutzer", MySqlDbType.VarChar).Value = konto.Benutzer;
            cmd.Parameters.Add("@passwort", MySqlDbType.VarChar).Value = konto.Passwort;
            cmd.Parameters.Add("@geburtsdatum", MySqlDbType.Date).Value = konto.Geburtsdatum;
            cmd.Parameters.Add("@adresse", MySqlDbType.VarChar).Value = konto.Adresse;
            cmd.Parameters.Add("@ort", MySqlDbType.VarChar).Value = konto.Ort;
            cmd.Parameters.Add("@postleitzahl", MySqlDbType.VarChar).Value = konto.Postleitzahl;
            cmd.Parameters.Add("@familienstatus", MySqlDbType.VarChar).Value = konto.Familienstatus;
            cmd.Parameters.Add("@telefonnummer", MySqlDbType.VarChar).Value = konto.Telefonnummer;
            cmd.Parameters.Add("@email", MySqlDbType.VarChar).Value = konto.Email;
            cmd.Parameters.Add("@blutgruppe", MySqlDbType.VarChar).Value = konto.Blutgruppe;
            cmd.Parameters.Add("@haarfarbe", MySqlDbType.VarChar).Value = konto.Haarfarbe;
            cmd.Parameters.Add("@augenfarbe", MySqlDbType.VarChar).Value = konto.Augenfarbe;
            cmd.Parameters.Add("@brille", MySqlDbType.VarChar).Value = konto.Brille;

            try
            {
                dc.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Sie haben erfolgreich ein neues Konto erstellt. Melden Sie sich jetzt an!");
                dc.Close();
            }
            catch
            {
                MessageBox.Show("Error1");
            }
        }
        public static void Highscore(string benutzer, int highscore, int level, int id)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query = "INSERT INTO tbl_highscore (benutzer, highscore, level, idd) VALUES(@benutzer, @highscore, @level, @id);";
            MySqlCommand cmd = new MySqlCommand(query, dc);

            cmd.Parameters.Add("@benutzer", MySqlDbType.VarChar).Value = benutzer;
            cmd.Parameters.Add("@highscore", MySqlDbType.Int32).Value = highscore;
            cmd.Parameters.Add("@level", MySqlDbType.Int32).Value = level;
            cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = id;

            try
            {
                dc.Open();
                cmd.ExecuteNonQuery();
                dc.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static void LoadData(List<cls_konto> list, int lvl, string alle, string benutzer)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query;
            if (alle == "ich")
            {
                if (lvl == 0)
                {
                    query = "SELECT * FROM tbl_highscore WHERE benutzer=@benutzer ORDER BY highscore DESC";
                }
                else
                {
                    query = "SELECT * FROM tbl_highscore WHERE level=@level AND benutzer=@benutzer ORDER BY highscore DESC";
                }
            }
            else
            {
                if (lvl == 0)
                {
                    query = "SELECT * FROM tbl_highscore ORDER BY highscore DESC";
                }
                else
                {
                    query = "SELECT * FROM tbl_highscore WHERE level=@level ORDER BY highscore DESC";
                }

            }
            MySqlCommand cmd = new MySqlCommand(query, dc);
            cmd.Parameters.Add("@level", MySqlDbType.Int32 ).Value = lvl;
            cmd.Parameters.Add("@benutzer", MySqlDbType.String ).Value = benutzer;

            try
            {
                dc.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cls_konto k = new cls_konto((dr[1]).ToString(), Convert.ToInt32(dr[2]), Convert.ToInt32(dr[3]), (dr[4].ToString()));
                    list.Add(k);
                }
                dc.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static void DeleteSignup(string benutzer, string passwort)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query = string.Format("DELETE FROM tbl_signup WHERE benutzer=@benutzer AND passwort=@passwort");
            MySqlCommand cmd = new MySqlCommand(query, dc);

            cmd.Parameters.Add("@benutzer", MySqlDbType.VarChar).Value = benutzer;
            cmd.Parameters.Add("@passwort", MySqlDbType.VarChar).Value = passwort;

            try
            {
                dc.Open();
                cmd.ExecuteNonQuery();
                dc.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static void DeleteHighscore(int id)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query = string.Format("DELETE FROM tbl_highscore WHERE idd=@id");
            MySqlCommand cmd = new MySqlCommand(query, dc);

            cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = id;

            try
            {
                dc.Open();
                cmd.ExecuteNonQuery();
                dc.Close();
                MessageBox.Show("Ihre Elemente sind gelöscht!", "Daten löschen");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static int GetID(string benutzer, string passwort)
        {
            MySqlConnection dc = new MySqlConnection(connstring);
            string query = "SELECT id FROM tbl_signup WHERE benutzer=@benutzer AND passwort=@passwort";

            
            MySqlCommand cmd = new MySqlCommand(query, dc);

            cmd.Parameters.Add("@benutzer", MySqlDbType.VarChar).Value = benutzer;
            cmd.Parameters.Add("@passwort", MySqlDbType.VarChar).Value = passwort;

            int idd = 0;
            try
            {
                dc.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        idd = dr.GetInt32(0);
                    }
                }
                dr.Close();
                dc.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return idd;
        }
    }
}
