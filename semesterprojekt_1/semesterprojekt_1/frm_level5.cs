using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace semesterprojekt_1
{
    public partial class frm_level5 : Form
    {
        int count = 0;
        int zeit;
        int highscore;
        string ben;
        int level = 5;
        bool lvl5 = false;
        int id;
        int leben;
        List<string> lander = new List<string> { "Uruguay", "Chile", "Jamaika","Litauen","Neuseeland","Panama","Südafrika","Serbien","Uganda","Gambia","Afghanistan" };
        public frm_level5()
        {
            InitializeComponent();
        }
        public frm_level5(string benutzer, int lb, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lb;
            id = idd;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < lander.Count; i++)
            {
                if (lander[i] == tbx_eingabe.Text)
                {
                    highscore++;
                    count++;
                    lbl_count.Text = string.Format("{0}/11", count);
                    lbl_highscore.Text = string.Format("Highscore: " + highscore);
                    tbx_eingabe.Text = "";
                    lander.Remove(lander[i]);
                    if (count == 11)
                    {
                        lvl5 = true;
                        tmr_lvl5.Stop();
                        highscore = zeit + highscore;
                        MessageBox.Show("Du hast es geschafft!" +
                            "Dein Highscore beträgt " + highscore);
                        cls_dataprovider.Highscore(ben, highscore, level, id);
                        cls_spiel.Leben = leben;
                        cls_spiel.Highscore = highscore;
                        cls_spiel.Level = level;
                        cls_spiel.Main = lvl5;
                        cls_spiel.Highscorelvl5 = highscore;
                        this.Close();
                    }
                }
            }
        }

        private void tmr_lvl5_Tick(object sender, EventArgs e)
        {
            lbl_zeit.Text = "Zeit:" + zeit--.ToString();

            if (zeit < 1)
            {
                lvl5 = true;
                tmr_lvl5.Stop();
                MessageBox.Show("Die Zeit ist aus.");
                leben++;
                cls_spiel.Leben = leben;
                cls_spiel.Highscore = highscore;
                cls_spiel.Level = level;
                cls_spiel.Main = lvl5;
                this.Close();
            }
        }

        private void frm_level5_Load(object sender, EventArgs e)
        {
            zeit = 240;
            tmr_lvl5.Start();
        }

        private void btn_aufgeben_Click(object sender, EventArgs e)
        {
            lvl5 = true;
            tmr_lvl5.Stop();
            MessageBox.Show("Warum hörst du auf?", "Willst du wirklich aufgeben?");
            leben++;
            cls_spiel.Leben = leben;
            cls_spiel.Highscore = highscore;
            cls_spiel.Level = level;
            cls_spiel.Main = lvl5;
            this.Close();
        }

        private void lbl_count_Click(object sender, EventArgs e)
        {

        }

        private void lbl_aufgabe_Click(object sender, EventArgs e)
        {

        }
    }
}
