﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;

namespace semesterprojekt_1
{
    public partial class frm_lvl4 : Form
    {
        string ben;
        int leben;
        int id;
        int count;
        int level = 4;
        int var;
        int high;
        int land;
        SoundPlayer player = new SoundPlayer(@"C:\musik\schweden.wav");
        SoundPlayer player2 = new SoundPlayer(@"C:\musik\taiwan.wav");
        SoundPlayer player3 = new SoundPlayer(@"C:\musik\ghana.wav");
        SoundPlayer player4 = new SoundPlayer(@"C:\musik\griechenland.wav");
        SoundPlayer player5 = new SoundPlayer(@"C:\musik\zypern.wav");
        SoundPlayer player6 = new SoundPlayer(@"C:\musik\honduras.wav");
        SoundPlayer player7 = new SoundPlayer(@"C:\musik\georgien.wav");
        SoundPlayer player8 = new SoundPlayer(@"C:\musik\nauru.wav");
        public frm_lvl4()
        {
            InitializeComponent();
        }
        public frm_lvl4(string benutzer, int lb, int idd)
        {
            InitializeComponent();
            ben = benutzer;
            leben = lb;
            id = idd;
        }

        private void frm_lvl4_Load(object sender, EventArgs e)
        {
            Spiel();
            lbl_highscore.Text = "Highscore: " + high;
        }
        
        private void Spiel()
        {
                if (count==0)
                {
                player.Play();
                btn_a1.Text = "Schweden";
                    btn_a2.Text = "Dänemark";
                    btn_a3.Text = "Norwegen";
                    if (var == 1)
                    {
                        player.Stop();
                        var = 0;
                        high = high + 10;
                        count++;
                    }
                    else if (var == 2 || var == 3)
                    {
                        high = high - 20;
                    }
                }
                if (count==1)
                {
                    player2.Play();
                    btn_a1.Text = "Japan";
                    btn_a2.Text = "Südkorea";
                    btn_a3.Text = "Taiwan";
                    if (var == 3)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player2.Stop();
                    }
                    else if (var == 2 || var == 1)
                    {
                        high = high - 20;
                    }
                }
                if (count==2)
                {
                    player3.Play();
                    btn_a1.Text = "Ghana";
                    btn_a2.Text = "Südafrika";
                    btn_a3.Text = "Tschad";
                    if (var == 1)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player3.Stop();
                    }
                    else if (var == 2 || var == 3)
                    {
                        high = high - 20;
                    }
                }
                if (count==3)
                {
                    player4.Play();
                    btn_a1.Text = "Elfenbeinküste";
                    btn_a2.Text = "Griechenland";
                    btn_a3.Text = "Samoa";
                    if (var == 2)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player4.Stop();
                    }
                    else if (var == 1 || var == 3)
                    {
                        high = high - 20;
                    }
                }
                if (count==4)
                {
                    player5.Play();
                    btn_a1.Text = "Island";
                    btn_a2.Text = "Zypern";
                    btn_a3.Text = "Malta";
                    if (var == 2)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player5.Stop();
                    }
                    else if (var == 1 || var == 3)
                    {
                        high = high - 20;
                    }
                }
                if (count==5)
                {
                    player6.Play();
                    btn_a1.Text = "Mexiko";
                    btn_a2.Text = "USA";
                    btn_a3.Text = "Honduras";
                    if (var == 3)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player6.Stop();
                    }
                    else if (var == 2 || var == 1)
                    {
                        high = high - 20;
                    }
                }
                if (count==6)
                {
                    player7.Play();
                    btn_a1.Text = "Georgien";
                    btn_a2.Text = "Armenien";
                    btn_a3.Text = "Aserbaidschan";
                    if (var == 1)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player7.Stop();
                    }
                    else if (var == 2 || var == 3)
                    {
                        high = high - 20;
                    }
                }
                if (count==7)
                {
                    player8.Play();
                    btn_a1.Text = "Kiribati";
                    btn_a2.Text = "Salomonen";
                    btn_a3.Text = "Nauru";
                    if (var == 3)
                    {
                        count++;
                        high = high + 10;
                        var = 0;
                        player8.Stop();
                    }
                    else if (var == 2 || var == 1)
                    {
                        high = high - 20;
                    }
                }
            
                lbl_highscore.Text = "Highscore: " + high;
            Ende();
        }
        private void Ende()
        {
            if (count == 8)
            {
                if (high > 0)
                {
                    if (high == 80)
                    {
                        high = high + 50;
                        MessageBox.Show("Du hast gewonnen! Da du alles richtig hattest, bekommst du 50 Zusatzpunkte! Dein Highscore beträgt " + high);
                        cls_dataprovider.Highscore(ben, high, level, id);
                        cls_spiel.Leben = leben;
                        cls_spiel.Highscore = high;
                        cls_spiel.Level = level;
                        cls_spiel.Highscorelvl4 = high;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Du hast gewonnen! Dein Highscore beträgt " + high);
                        cls_dataprovider.Highscore(ben, high, level, id);
                        cls_spiel.Leben = leben;
                        cls_spiel.Highscore = high;
                        cls_spiel.Level = level;
                        cls_spiel.Highscorelvl4 = high;
                        this.Close();
                    }
                }
                else
                {
                    leben++;
                    cls_spiel.Leben= leben; 
                    MessageBox.Show("Dein Highscore ist negativ, du hast es nicht geschafft!");
                    this.Close();
                }
            }
        }
        private void btn_a1_Click(object sender, EventArgs e)
        {
            var = 1;
            Spiel();
        }

        private void btn_a2_Click(object sender, EventArgs e)
        {
            var = 2;
            Spiel();
        }

        private void btn_a3_Click(object sender, EventArgs e)
        {
            var = 3;
            Spiel();
        }

        private void btn_aufgeben_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Warum hörst du auf?", "Willst du wirklich aufgeben?");
            player.Stop();
            player2.Stop();
            player3.Stop();
            player4.Stop();
            player5.Stop();
            player6.Stop();
            player7.Stop();
            player8.Stop();
            leben++;
            cls_spiel.Leben = leben;
            cls_spiel.Highscore = high;
            cls_spiel.Level = level;
            this.Close();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }
    }
}
