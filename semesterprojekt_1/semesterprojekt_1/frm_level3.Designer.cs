﻿namespace semesterprojekt_1
{
    partial class frm_level3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmr_lvl3 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_hs = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.lbl_nordkorea = new System.Windows.Forms.Label();
            this.lbl_laos = new System.Windows.Forms.Label();
            this.lbl_jordanien = new System.Windows.Forms.Label();
            this.pb_bangladesh = new System.Windows.Forms.PictureBox();
            this.pb_jordanien = new System.Windows.Forms.PictureBox();
            this.pb_r = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pb_ghana = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pb_kapverde = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pb_burundi = new System.Windows.Forms.PictureBox();
            this.pb_laos = new System.Windows.Forms.PictureBox();
            this.pb_aegypten = new System.Windows.Forms.PictureBox();
            this.pb_marokko = new System.Windows.Forms.PictureBox();
            this.pb_nordkorea = new System.Windows.Forms.PictureBox();
            this.lbl_bangladesh = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_bangladesh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_jordanien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ghana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kapverde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_burundi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_laos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_aegypten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_marokko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nordkorea)).BeginInit();
            this.SuspendLayout();
            // 
            // tmr_lvl3
            // 
            this.tmr_lvl3.Enabled = true;
            this.tmr_lvl3.Interval = 25;
            this.tmr_lvl3.Tick += new System.EventHandler(this.tmr_lvl3_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welche Länder gehören zu Afrika?";
            // 
            // lbl_hs
            // 
            this.lbl_hs.AutoSize = true;
            this.lbl_hs.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hs.Location = new System.Drawing.Point(11, 122);
            this.lbl_hs.Name = "lbl_hs";
            this.lbl_hs.Size = new System.Drawing.Size(33, 35);
            this.lbl_hs.TabIndex = 5;
            this.lbl_hs.Text = "0";
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_count.Location = new System.Drawing.Point(12, 166);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(42, 26);
            this.lbl_count.TabIndex = 7;
            this.lbl_count.Text = "0/5";
            // 
            // lbl_nordkorea
            // 
            this.lbl_nordkorea.AutoSize = true;
            this.lbl_nordkorea.Font = new System.Drawing.Font("Rockwell", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nordkorea.ForeColor = System.Drawing.Color.Red;
            this.lbl_nordkorea.Location = new System.Drawing.Point(3, 531);
            this.lbl_nordkorea.Name = "lbl_nordkorea";
            this.lbl_nordkorea.Size = new System.Drawing.Size(84, 17);
            this.lbl_nordkorea.TabIndex = 15;
            this.lbl_nordkorea.Text = "Nordkorea";
            this.lbl_nordkorea.Visible = false;
            // 
            // lbl_laos
            // 
            this.lbl_laos.AutoSize = true;
            this.lbl_laos.Font = new System.Drawing.Font("Rockwell", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_laos.ForeColor = System.Drawing.Color.Red;
            this.lbl_laos.Location = new System.Drawing.Point(3, 503);
            this.lbl_laos.Name = "lbl_laos";
            this.lbl_laos.Size = new System.Drawing.Size(40, 17);
            this.lbl_laos.TabIndex = 16;
            this.lbl_laos.Text = "Laos";
            this.lbl_laos.Visible = false;
            // 
            // lbl_jordanien
            // 
            this.lbl_jordanien.AutoSize = true;
            this.lbl_jordanien.Font = new System.Drawing.Font("Rockwell", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_jordanien.ForeColor = System.Drawing.Color.Red;
            this.lbl_jordanien.Location = new System.Drawing.Point(3, 474);
            this.lbl_jordanien.Name = "lbl_jordanien";
            this.lbl_jordanien.Size = new System.Drawing.Size(77, 17);
            this.lbl_jordanien.TabIndex = 17;
            this.lbl_jordanien.Text = "Jordanien";
            this.lbl_jordanien.Visible = false;
            // 
            // pb_bangladesh
            // 
            this.pb_bangladesh.Image = global::semesterprojekt_1.Properties.Resources.bangladesch1;
            this.pb_bangladesh.Location = new System.Drawing.Point(157, 590);
            this.pb_bangladesh.Name = "pb_bangladesh";
            this.pb_bangladesh.Size = new System.Drawing.Size(64, 34);
            this.pb_bangladesh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_bangladesh.TabIndex = 18;
            this.pb_bangladesh.TabStop = false;
            this.pb_bangladesh.Tag = "richtig";
            // 
            // pb_jordanien
            // 
            this.pb_jordanien.Image = global::semesterprojekt_1.Properties.Resources.jordanien;
            this.pb_jordanien.Location = new System.Drawing.Point(323, 580);
            this.pb_jordanien.Name = "pb_jordanien";
            this.pb_jordanien.Size = new System.Drawing.Size(64, 34);
            this.pb_jordanien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_jordanien.TabIndex = 14;
            this.pb_jordanien.TabStop = false;
            this.pb_jordanien.Tag = "richtig";
            // 
            // pb_r
            // 
            this.pb_r.Image = global::semesterprojekt_1.Properties.Resources.froggy;
            this.pb_r.Location = new System.Drawing.Point(270, 456);
            this.pb_r.Name = "pb_r";
            this.pb_r.Size = new System.Drawing.Size(72, 92);
            this.pb_r.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_r.TabIndex = 0;
            this.pb_r.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::semesterprojekt_1.Properties.Resources.gras1;
            this.pictureBox2.Location = new System.Drawing.Point(-73, 554);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(770, 187);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pb_ghana
            // 
            this.pb_ghana.Image = global::semesterprojekt_1.Properties.Resources.ghana;
            this.pb_ghana.Location = new System.Drawing.Point(23, 580);
            this.pb_ghana.Name = "pb_ghana";
            this.pb_ghana.Size = new System.Drawing.Size(64, 34);
            this.pb_ghana.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_ghana.TabIndex = 8;
            this.pb_ghana.TabStop = false;
            this.pb_ghana.Tag = "falsch";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::semesterprojekt_1.Properties.Resources.wolke;
            this.pictureBox1.Location = new System.Drawing.Point(-51, -5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(426, 102);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pb_kapverde
            // 
            this.pb_kapverde.Image = global::semesterprojekt_1.Properties.Resources.kap_verde;
            this.pb_kapverde.Location = new System.Drawing.Point(48, 590);
            this.pb_kapverde.Name = "pb_kapverde";
            this.pb_kapverde.Size = new System.Drawing.Size(64, 34);
            this.pb_kapverde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_kapverde.TabIndex = 10;
            this.pb_kapverde.TabStop = false;
            this.pb_kapverde.Tag = "falsch";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::semesterprojekt_1.Properties.Resources.wolke;
            this.pictureBox3.Location = new System.Drawing.Point(355, -5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(426, 102);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // pb_burundi
            // 
            this.pb_burundi.Image = global::semesterprojekt_1.Properties.Resources.burundi;
            this.pb_burundi.Location = new System.Drawing.Point(489, 40);
            this.pb_burundi.Name = "pb_burundi";
            this.pb_burundi.Size = new System.Drawing.Size(64, 34);
            this.pb_burundi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_burundi.TabIndex = 3;
            this.pb_burundi.TabStop = false;
            this.pb_burundi.Tag = "richtig";
            // 
            // pb_laos
            // 
            this.pb_laos.Image = global::semesterprojekt_1.Properties.Resources.laos1;
            this.pb_laos.Location = new System.Drawing.Point(489, 580);
            this.pb_laos.Name = "pb_laos";
            this.pb_laos.Size = new System.Drawing.Size(64, 34);
            this.pb_laos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_laos.TabIndex = 4;
            this.pb_laos.TabStop = false;
            this.pb_laos.Tag = "falsch";
            // 
            // pb_aegypten
            // 
            this.pb_aegypten.Image = global::semesterprojekt_1.Properties.Resources.aegypten;
            this.pb_aegypten.Location = new System.Drawing.Point(48, 29);
            this.pb_aegypten.Name = "pb_aegypten";
            this.pb_aegypten.Size = new System.Drawing.Size(64, 34);
            this.pb_aegypten.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_aegypten.TabIndex = 6;
            this.pb_aegypten.TabStop = false;
            this.pb_aegypten.Tag = "falsch";
            // 
            // pb_marokko
            // 
            this.pb_marokko.Image = global::semesterprojekt_1.Properties.Resources.marokko;
            this.pb_marokko.Location = new System.Drawing.Point(270, 619);
            this.pb_marokko.Name = "pb_marokko";
            this.pb_marokko.Size = new System.Drawing.Size(64, 34);
            this.pb_marokko.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_marokko.TabIndex = 11;
            this.pb_marokko.TabStop = false;
            this.pb_marokko.Tag = "falsch";
            // 
            // pb_nordkorea
            // 
            this.pb_nordkorea.Image = global::semesterprojekt_1.Properties.Resources.korea_nord;
            this.pb_nordkorea.Location = new System.Drawing.Point(514, 580);
            this.pb_nordkorea.Name = "pb_nordkorea";
            this.pb_nordkorea.Size = new System.Drawing.Size(64, 34);
            this.pb_nordkorea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_nordkorea.TabIndex = 13;
            this.pb_nordkorea.TabStop = false;
            this.pb_nordkorea.Tag = "richtig";
            // 
            // lbl_bangladesh
            // 
            this.lbl_bangladesh.AutoSize = true;
            this.lbl_bangladesh.Font = new System.Drawing.Font("Rockwell", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bangladesh.ForeColor = System.Drawing.Color.Red;
            this.lbl_bangladesh.Location = new System.Drawing.Point(3, 447);
            this.lbl_bangladesh.Name = "lbl_bangladesh";
            this.lbl_bangladesh.Size = new System.Drawing.Size(98, 17);
            this.lbl_bangladesh.TabIndex = 19;
            this.lbl_bangladesh.Text = "Bangladesch";
            this.lbl_bangladesh.Visible = false;
            // 
            // frm_level3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.ClientSize = new System.Drawing.Size(630, 649);
            this.Controls.Add(this.lbl_bangladesh);
            this.Controls.Add(this.lbl_jordanien);
            this.Controls.Add(this.lbl_laos);
            this.Controls.Add(this.lbl_nordkorea);
            this.Controls.Add(this.pb_r);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pb_ghana);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.lbl_hs);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pb_kapverde);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pb_burundi);
            this.Controls.Add(this.pb_laos);
            this.Controls.Add(this.pb_aegypten);
            this.Controls.Add(this.pb_marokko);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pb_nordkorea);
            this.Controls.Add(this.pb_bangladesh);
            this.Controls.Add(this.pb_jordanien);
            this.Name = "frm_level3";
            this.Text = "frm_level3";
            this.Load += new System.EventHandler(this.frm_level3_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_level3_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frm_level3_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pb_bangladesh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_jordanien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_r)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ghana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_kapverde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_burundi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_laos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_aegypten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_marokko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_nordkorea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_r;
        private System.Windows.Forms.Timer tmr_lvl3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pb_burundi;
        private System.Windows.Forms.PictureBox pb_laos;
        private System.Windows.Forms.Label lbl_hs;
        private System.Windows.Forms.PictureBox pb_aegypten;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.PictureBox pb_ghana;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pb_kapverde;
        private System.Windows.Forms.PictureBox pb_marokko;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pb_nordkorea;
        private System.Windows.Forms.PictureBox pb_jordanien;
        private System.Windows.Forms.Label lbl_nordkorea;
        private System.Windows.Forms.Label lbl_laos;
        private System.Windows.Forms.Label lbl_jordanien;
        private System.Windows.Forms.PictureBox pb_bangladesh;
        private System.Windows.Forms.Label lbl_bangladesh;
    }
}