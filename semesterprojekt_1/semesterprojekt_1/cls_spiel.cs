﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace semesterprojekt_1
{
    static class cls_spiel
    {
        static bool main;
        static int level;
        static int highscore;
        static int highscorelvl1;
        static int highscorelvl2;
        static int highscorelvl3;
        static int highscorelvl4;
        static int highscorelvl5;
        static int highscorelvl6;
        static int leben;

        public static bool Main { get => main; set => main = value; }
        public static int Level { get => level; set => level = value; }
        public static int Highscore { get => highscore; set => highscore = value; }
        public static int Leben { get => leben; set => leben = value; }
        public static int Highscorelvl1 { get => highscorelvl1; set => highscorelvl1 = value; }
        public static int Highscorelvl2 { get => highscorelvl2; set => highscorelvl2 = value; }
        public static int Highscorelvl3 { get => highscorelvl3; set => highscorelvl3 = value; }
        public static int Highscorelvl4 { get => highscorelvl4; set => highscorelvl4 = value; }
        public static int Highscorelvl5 { get => highscorelvl5; set => highscorelvl5 = value; }
        public static int Highscorelvl6 { get => highscorelvl6; set => highscorelvl6 = value; }
    }
}
