namespace semesterprojekt_1
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_benutzer = new System.Windows.Forms.Label();
            this.lbl_passwort = new System.Windows.Forms.Label();
            this.tbx_benutzer = new System.Windows.Forms.TextBox();
            this.tbx_passwort = new System.Windows.Forms.TextBox();
            this.btn_loschen = new System.Windows.Forms.Button();
            this.btn_login = new System.Windows.Forms.Button();
            this.btn_signup = new System.Windows.Forms.Button();
            this.lbx_board = new System.Windows.Forms.ListBox();
            this.lbl_name = new System.Windows.Forms.Label();
            this.btn_ausloggen = new System.Windows.Forms.Button();
            this.cbx_level = new System.Windows.Forms.ComboBox();
            this.btn_account = new System.Windows.Forms.Button();
            this.btn_daten = new System.Windows.Forms.Button();
            this.rbtn_alle = new System.Windows.Forms.RadioButton();
            this.rbtn_ich = new System.Windows.Forms.RadioButton();
            this.btn_zusatz1 = new System.Windows.Forms.Button();
            this.btn_zusatzlvl2 = new System.Windows.Forms.Button();
            this.pbx_5 = new System.Windows.Forms.PictureBox();
            this.pbx_4 = new System.Windows.Forms.PictureBox();
            this.pbx_2 = new System.Windows.Forms.PictureBox();
            this.pbx_1 = new System.Windows.Forms.PictureBox();
            this.pbx_3 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl6 = new System.Windows.Forms.PictureBox();
            this.pbx_zlvl2 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl3 = new System.Windows.Forms.PictureBox();
            this.pbx_zlvl1 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl2 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl1 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl5 = new System.Windows.Forms.PictureBox();
            this.pbx_lvl4 = new System.Windows.Forms.PictureBox();
            this.pbx_grau3 = new System.Windows.Forms.PictureBox();
            this.pbx_grau2 = new System.Windows.Forms.PictureBox();
            this.pbx_grau1 = new System.Windows.Forms.PictureBox();
            this.pbx_rot1 = new System.Windows.Forms.PictureBox();
            this.pbx_rot2 = new System.Windows.Forms.PictureBox();
            this.pbx_rot3 = new System.Windows.Forms.PictureBox();
            this.pbx_avatar = new System.Windows.Forms.PictureBox();
            this.pbx_6 = new System.Windows.Forms.PictureBox();
            this.pbx_7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_zlvl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_zlvl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_avatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_7)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_benutzer
            // 
            this.lbl_benutzer.AutoSize = true;
            this.lbl_benutzer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_benutzer.Location = new System.Drawing.Point(9, 10);
            this.lbl_benutzer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_benutzer.Name = "lbl_benutzer";
            this.lbl_benutzer.Size = new System.Drawing.Size(52, 13);
            this.lbl_benutzer.TabIndex = 0;
            this.lbl_benutzer.Text = "Benutzer:";
            // 
            // lbl_passwort
            // 
            this.lbl_passwort.AutoSize = true;
            this.lbl_passwort.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_passwort.Location = new System.Drawing.Point(9, 34);
            this.lbl_passwort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_passwort.Name = "lbl_passwort";
            this.lbl_passwort.Size = new System.Drawing.Size(53, 13);
            this.lbl_passwort.TabIndex = 1;
            this.lbl_passwort.Text = "Passwort:";
            // 
            // tbx_benutzer
            // 
            this.tbx_benutzer.Location = new System.Drawing.Point(92, 10);
            this.tbx_benutzer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_benutzer.Name = "tbx_benutzer";
            this.tbx_benutzer.Size = new System.Drawing.Size(76, 20);
            this.tbx_benutzer.TabIndex = 1;
            // 
            // tbx_passwort
            // 
            this.tbx_passwort.Location = new System.Drawing.Point(92, 34);
            this.tbx_passwort.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_passwort.Name = "tbx_passwort";
            this.tbx_passwort.PasswordChar = '*';
            this.tbx_passwort.Size = new System.Drawing.Size(76, 20);
            this.tbx_passwort.TabIndex = 2;
            // 
            // btn_loschen
            // 
            this.btn_loschen.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_loschen.Location = new System.Drawing.Point(9, 60);
            this.btn_loschen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_loschen.Name = "btn_loschen";
            this.btn_loschen.Size = new System.Drawing.Size(71, 28);
            this.btn_loschen.TabIndex = 3;
            this.btn_loschen.Text = "Löschen";
            this.btn_loschen.UseVisualStyleBackColor = true;
            this.btn_loschen.Click += new System.EventHandler(this.btn_loschen_Click);
            // 
            // btn_login
            // 
            this.btn_login.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_login.Location = new System.Drawing.Point(92, 59);
            this.btn_login.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(75, 29);
            this.btn_login.TabIndex = 4;
            this.btn_login.Text = "Log in";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // btn_signup
            // 
            this.btn_signup.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_signup.Location = new System.Drawing.Point(52, 93);
            this.btn_signup.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_signup.Name = "btn_signup";
            this.btn_signup.Size = new System.Drawing.Size(65, 27);
            this.btn_signup.TabIndex = 6;
            this.btn_signup.Text = "Sign Up";
            this.btn_signup.UseVisualStyleBackColor = true;
            this.btn_signup.Click += new System.EventHandler(this.btn_signup_Click);
            // 
            // lbx_board
            // 
            this.lbx_board.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.lbx_board.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lbx_board.FormattingEnabled = true;
            this.lbx_board.Location = new System.Drawing.Point(196, 10);
            this.lbx_board.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lbx_board.Name = "lbx_board";
            this.lbx_board.Size = new System.Drawing.Size(627, 82);
            this.lbx_board.TabIndex = 7;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_name.Location = new System.Drawing.Point(9, 72);
            this.lbl_name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(35, 13);
            this.lbl_name.TabIndex = 9;
            this.lbl_name.Text = "label1";
            this.lbl_name.Visible = false;
            // 
            // btn_ausloggen
            // 
            this.btn_ausloggen.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_ausloggen.Location = new System.Drawing.Point(81, 72);
            this.btn_ausloggen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ausloggen.Name = "btn_ausloggen";
            this.btn_ausloggen.Size = new System.Drawing.Size(104, 26);
            this.btn_ausloggen.TabIndex = 12;
            this.btn_ausloggen.Text = "Ausloggen";
            this.btn_ausloggen.UseVisualStyleBackColor = true;
            this.btn_ausloggen.Visible = false;
            this.btn_ausloggen.Click += new System.EventHandler(this.btn_ausloggen_Click);
            // 
            // cbx_level
            // 
            this.cbx_level.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cbx_level.FormattingEnabled = true;
            this.cbx_level.Items.AddRange(new object[] {
            "Alle",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbx_level.Location = new System.Drawing.Point(734, 79);
            this.cbx_level.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbx_level.Name = "cbx_level";
            this.cbx_level.Size = new System.Drawing.Size(92, 21);
            this.cbx_level.TabIndex = 20;
            this.cbx_level.TextChanged += new System.EventHandler(this.cbx_level_TextChanged);
            // 
            // btn_account
            // 
            this.btn_account.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_account.Location = new System.Drawing.Point(81, 40);
            this.btn_account.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_account.Name = "btn_account";
            this.btn_account.Size = new System.Drawing.Size(104, 26);
            this.btn_account.TabIndex = 21;
            this.btn_account.Text = "Account löschen";
            this.btn_account.UseVisualStyleBackColor = true;
            this.btn_account.Visible = false;
            this.btn_account.Click += new System.EventHandler(this.btn_account_Click);
            // 
            // btn_daten
            // 
            this.btn_daten.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_daten.Location = new System.Drawing.Point(81, 10);
            this.btn_daten.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_daten.Name = "btn_daten";
            this.btn_daten.Size = new System.Drawing.Size(104, 24);
            this.btn_daten.TabIndex = 22;
            this.btn_daten.Text = "Daten löschen";
            this.btn_daten.UseVisualStyleBackColor = true;
            this.btn_daten.Visible = false;
            this.btn_daten.Click += new System.EventHandler(this.btn_daten_Click);
            // 
            // rbtn_alle
            // 
            this.rbtn_alle.AutoSize = true;
            this.rbtn_alle.Location = new System.Drawing.Point(734, 104);
            this.rbtn_alle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtn_alle.Name = "rbtn_alle";
            this.rbtn_alle.Size = new System.Drawing.Size(88, 17);
            this.rbtn_alle.TabIndex = 23;
            this.rbtn_alle.TabStop = true;
            this.rbtn_alle.Text = "Alle anzeigen";
            this.rbtn_alle.UseVisualStyleBackColor = true;
            this.rbtn_alle.Visible = false;
            this.rbtn_alle.CheckedChanged += new System.EventHandler(this.rbtn_alle_CheckedChanged);
            // 
            // rbtn_ich
            // 
            this.rbtn_ich.AutoSize = true;
            this.rbtn_ich.Location = new System.Drawing.Point(734, 128);
            this.rbtn_ich.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtn_ich.Name = "rbtn_ich";
            this.rbtn_ich.Size = new System.Drawing.Size(70, 30);
            this.rbtn_ich.TabIndex = 23;
            this.rbtn_ich.TabStop = true;
            this.rbtn_ich.Text = "Nur mich \r\nanzeigen";
            this.rbtn_ich.UseVisualStyleBackColor = true;
            this.rbtn_ich.Visible = false;
            this.rbtn_ich.CheckedChanged += new System.EventHandler(this.rbtn_ich_CheckedChanged);
            // 
            // btn_zusatz1
            // 
            this.btn_zusatz1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_zusatz1.Location = new System.Drawing.Point(232, 277);
            this.btn_zusatz1.Margin = new System.Windows.Forms.Padding(2);
            this.btn_zusatz1.Name = "btn_zusatz1";
            this.btn_zusatz1.Size = new System.Drawing.Size(101, 32);
            this.btn_zusatz1.TabIndex = 24;
            this.btn_zusatz1.Text = "Zusatzlevel 1";
            this.btn_zusatz1.UseVisualStyleBackColor = true;
            // 
            // btn_zusatzlvl2
            // 
            this.btn_zusatzlvl2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_zusatzlvl2.Location = new System.Drawing.Point(587, 464);
            this.btn_zusatzlvl2.Margin = new System.Windows.Forms.Padding(2);
            this.btn_zusatzlvl2.Name = "btn_zusatzlvl2";
            this.btn_zusatzlvl2.Size = new System.Drawing.Size(101, 32);
            this.btn_zusatzlvl2.TabIndex = 25;
            this.btn_zusatzlvl2.Text = "Zusatzlevel 2";
            this.btn_zusatzlvl2.UseVisualStyleBackColor = true;
            // 
            // pbx_5
            // 
            this.pbx_5.BackColor = System.Drawing.Color.Transparent;
            this.pbx_5.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_rechts1;
            this.pbx_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_5.Location = new System.Drawing.Point(598, 422);
            this.pbx_5.Name = "pbx_5";
            this.pbx_5.Size = new System.Drawing.Size(128, 39);
            this.pbx_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_5.TabIndex = 32;
            this.pbx_5.TabStop = false;
            this.pbx_5.Visible = false;
            // 
            // pbx_4
            // 
            this.pbx_4.BackColor = System.Drawing.Color.Transparent;
            this.pbx_4.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_rechts1;
            this.pbx_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_4.Location = new System.Drawing.Point(403, 422);
            this.pbx_4.Name = "pbx_4";
            this.pbx_4.Size = new System.Drawing.Size(110, 33);
            this.pbx_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_4.TabIndex = 31;
            this.pbx_4.TabStop = false;
            this.pbx_4.Visible = false;
            // 
            // pbx_2
            // 
            this.pbx_2.BackColor = System.Drawing.Color.Transparent;
            this.pbx_2.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_oben;
            this.pbx_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_2.Location = new System.Drawing.Point(228, 354);
            this.pbx_2.Name = "pbx_2";
            this.pbx_2.Size = new System.Drawing.Size(25, 78);
            this.pbx_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_2.TabIndex = 29;
            this.pbx_2.TabStop = false;
            this.pbx_2.Visible = false;
            // 
            // pbx_1
            // 
            this.pbx_1.BackColor = System.Drawing.Color.Transparent;
            this.pbx_1.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_unten;
            this.pbx_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_1.Location = new System.Drawing.Point(81, 253);
            this.pbx_1.Name = "pbx_1";
            this.pbx_1.Size = new System.Drawing.Size(19, 119);
            this.pbx_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_1.TabIndex = 28;
            this.pbx_1.TabStop = false;
            this.pbx_1.Visible = false;
            // 
            // pbx_3
            // 
            this.pbx_3.BackColor = System.Drawing.Color.Transparent;
            this.pbx_3.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_rechts1;
            this.pbx_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_3.Location = new System.Drawing.Point(143, 422);
            this.pbx_3.Name = "pbx_3";
            this.pbx_3.Size = new System.Drawing.Size(178, 33);
            this.pbx_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_3.TabIndex = 26;
            this.pbx_3.TabStop = false;
            this.pbx_3.Visible = false;
            // 
            // pbx_lvl6
            // 
            this.pbx_lvl6.BackgroundImage = global::semesterprojekt_1.Properties.Resources.mqrs;
            this.pbx_lvl6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl6.Location = new System.Drawing.Point(688, 172);
            this.pbx_lvl6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl6.Name = "pbx_lvl6";
            this.pbx_lvl6.Size = new System.Drawing.Size(103, 82);
            this.pbx_lvl6.TabIndex = 41;
            this.pbx_lvl6.TabStop = false;
            this.pbx_lvl6.Visible = false;
            this.pbx_lvl6.Click += new System.EventHandler(this.pbx_lvl6_Click);
            // 
            // pbx_zlvl2
            // 
            this.pbx_zlvl2.BackgroundImage = global::semesterprojekt_1.Properties.Resources.fisch;
            this.pbx_zlvl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_zlvl2.Location = new System.Drawing.Point(498, 273);
            this.pbx_zlvl2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_zlvl2.Name = "pbx_zlvl2";
            this.pbx_zlvl2.Size = new System.Drawing.Size(118, 76);
            this.pbx_zlvl2.TabIndex = 40;
            this.pbx_zlvl2.TabStop = false;
            this.pbx_zlvl2.Visible = false;
            this.pbx_zlvl2.Click += new System.EventHandler(this.pbx_zlvl2_Click);
            // 
            // pbx_lvl3
            // 
            this.pbx_lvl3.BackgroundImage = global::semesterprojekt_1.Properties.Resources.afrika;
            this.pbx_lvl3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl3.Location = new System.Drawing.Point(323, 381);
            this.pbx_lvl3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl3.Name = "pbx_lvl3";
            this.pbx_lvl3.Size = new System.Drawing.Size(82, 96);
            this.pbx_lvl3.TabIndex = 39;
            this.pbx_lvl3.TabStop = false;
            this.pbx_lvl3.Visible = false;
            this.pbx_lvl3.Click += new System.EventHandler(this.pbx_lvl3_Click);
            // 
            // pbx_zlvl1
            // 
            this.pbx_zlvl1.BackgroundImage = global::semesterprojekt_1.Properties.Resources.tree;
            this.pbx_zlvl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_zlvl1.Location = new System.Drawing.Point(164, 260);
            this.pbx_zlvl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_zlvl1.Name = "pbx_zlvl1";
            this.pbx_zlvl1.Size = new System.Drawing.Size(157, 96);
            this.pbx_zlvl1.TabIndex = 38;
            this.pbx_zlvl1.TabStop = false;
            this.pbx_zlvl1.Visible = false;
            this.pbx_zlvl1.Click += new System.EventHandler(this.pbx_zlvl1_Click);
            // 
            // pbx_lvl2
            // 
            this.pbx_lvl2.BackgroundImage = global::semesterprojekt_1.Properties.Resources.flagge;
            this.pbx_lvl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl2.Location = new System.Drawing.Point(52, 364);
            this.pbx_lvl2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl2.Name = "pbx_lvl2";
            this.pbx_lvl2.Size = new System.Drawing.Size(113, 105);
            this.pbx_lvl2.TabIndex = 37;
            this.pbx_lvl2.TabStop = false;
            this.pbx_lvl2.Visible = false;
            this.pbx_lvl2.Click += new System.EventHandler(this.pbx_lvl2_Click);
            // 
            // pbx_lvl1
            // 
            this.pbx_lvl1.BackgroundImage = global::semesterprojekt_1.Properties.Resources.europa;
            this.pbx_lvl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl1.Location = new System.Drawing.Point(28, 163);
            this.pbx_lvl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl1.Name = "pbx_lvl1";
            this.pbx_lvl1.Size = new System.Drawing.Size(124, 98);
            this.pbx_lvl1.TabIndex = 36;
            this.pbx_lvl1.TabStop = false;
            this.pbx_lvl1.Visible = false;
            this.pbx_lvl1.Click += new System.EventHandler(this.pbx_lvl1_Click);
            // 
            // pbx_lvl5
            // 
            this.pbx_lvl5.BackgroundImage = global::semesterprojekt_1.Properties.Resources.rocket;
            this.pbx_lvl5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl5.Location = new System.Drawing.Point(701, 340);
            this.pbx_lvl5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl5.Name = "pbx_lvl5";
            this.pbx_lvl5.Size = new System.Drawing.Size(89, 130);
            this.pbx_lvl5.TabIndex = 35;
            this.pbx_lvl5.TabStop = false;
            this.pbx_lvl5.Visible = false;
            this.pbx_lvl5.Click += new System.EventHandler(this.pbx_lvl5_Click);
            // 
            // pbx_lvl4
            // 
            this.pbx_lvl4.BackgroundImage = global::semesterprojekt_1.Properties.Resources.zucker1;
            this.pbx_lvl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_lvl4.Location = new System.Drawing.Point(498, 381);
            this.pbx_lvl4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_lvl4.Name = "pbx_lvl4";
            this.pbx_lvl4.Size = new System.Drawing.Size(112, 96);
            this.pbx_lvl4.TabIndex = 34;
            this.pbx_lvl4.TabStop = false;
            this.pbx_lvl4.Visible = false;
            this.pbx_lvl4.Click += new System.EventHandler(this.pbx_lvl4_Click);
            // 
            // pbx_grau3
            // 
            this.pbx_grau3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_grau3.Image = global::semesterprojekt_1.Properties.Resources.herz_grau;
            this.pbx_grau3.Location = new System.Drawing.Point(755, 11);
            this.pbx_grau3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_grau3.Name = "pbx_grau3";
            this.pbx_grau3.Size = new System.Drawing.Size(53, 55);
            this.pbx_grau3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_grau3.TabIndex = 19;
            this.pbx_grau3.TabStop = false;
            this.pbx_grau3.Visible = false;
            // 
            // pbx_grau2
            // 
            this.pbx_grau2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_grau2.Image = global::semesterprojekt_1.Properties.Resources.herz_grau;
            this.pbx_grau2.Location = new System.Drawing.Point(694, 11);
            this.pbx_grau2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_grau2.Name = "pbx_grau2";
            this.pbx_grau2.Size = new System.Drawing.Size(53, 55);
            this.pbx_grau2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_grau2.TabIndex = 18;
            this.pbx_grau2.TabStop = false;
            this.pbx_grau2.Visible = false;
            // 
            // pbx_grau1
            // 
            this.pbx_grau1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_grau1.Image = global::semesterprojekt_1.Properties.Resources.herz_grau;
            this.pbx_grau1.Location = new System.Drawing.Point(634, 11);
            this.pbx_grau1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_grau1.Name = "pbx_grau1";
            this.pbx_grau1.Size = new System.Drawing.Size(53, 55);
            this.pbx_grau1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_grau1.TabIndex = 17;
            this.pbx_grau1.TabStop = false;
            this.pbx_grau1.Visible = false;
            // 
            // pbx_rot1
            // 
            this.pbx_rot1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_rot1.Image = global::semesterprojekt_1.Properties.Resources.herz_rot;
            this.pbx_rot1.Location = new System.Drawing.Point(634, 11);
            this.pbx_rot1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_rot1.Name = "pbx_rot1";
            this.pbx_rot1.Size = new System.Drawing.Size(53, 55);
            this.pbx_rot1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_rot1.TabIndex = 16;
            this.pbx_rot1.TabStop = false;
            // 
            // pbx_rot2
            // 
            this.pbx_rot2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_rot2.Image = global::semesterprojekt_1.Properties.Resources.herz_rot;
            this.pbx_rot2.Location = new System.Drawing.Point(694, 11);
            this.pbx_rot2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_rot2.Name = "pbx_rot2";
            this.pbx_rot2.Size = new System.Drawing.Size(53, 55);
            this.pbx_rot2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_rot2.TabIndex = 15;
            this.pbx_rot2.TabStop = false;
            // 
            // pbx_rot3
            // 
            this.pbx_rot3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pbx_rot3.Image = global::semesterprojekt_1.Properties.Resources.herz_rot;
            this.pbx_rot3.Location = new System.Drawing.Point(755, 11);
            this.pbx_rot3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbx_rot3.Name = "pbx_rot3";
            this.pbx_rot3.Size = new System.Drawing.Size(53, 55);
            this.pbx_rot3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_rot3.TabIndex = 14;
            this.pbx_rot3.TabStop = false;
            // 
            // pbx_avatar
            // 
            this.pbx_avatar.Image = global::semesterprojekt_1.Properties.Resources.froggy;
            this.pbx_avatar.Location = new System.Drawing.Point(9, 6);
            this.pbx_avatar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbx_avatar.Name = "pbx_avatar";
            this.pbx_avatar.Size = new System.Drawing.Size(66, 60);
            this.pbx_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_avatar.TabIndex = 8;
            this.pbx_avatar.TabStop = false;
            this.pbx_avatar.Visible = false;
            // 
            // pbx_6
            // 
            this.pbx_6.BackColor = System.Drawing.Color.Transparent;
            this.pbx_6.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_oben;
            this.pbx_6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_6.Location = new System.Drawing.Point(714, 260);
            this.pbx_6.Name = "pbx_6";
            this.pbx_6.Size = new System.Drawing.Size(58, 81);
            this.pbx_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_6.TabIndex = 27;
            this.pbx_6.TabStop = false;
            this.pbx_6.Visible = false;
            // 
            // pbx_7
            // 
            this.pbx_7.BackColor = System.Drawing.Color.Transparent;
            this.pbx_7.BackgroundImage = global::semesterprojekt_1.Properties.Resources.pfeil_links;
            this.pbx_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbx_7.Location = new System.Drawing.Point(605, 297);
            this.pbx_7.Name = "pbx_7";
            this.pbx_7.Size = new System.Drawing.Size(129, 27);
            this.pbx_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_7.TabIndex = 30;
            this.pbx_7.TabStop = false;
            this.pbx_7.Visible = false;
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(72)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(832, 505);
            this.Controls.Add(this.pbx_7);
            this.Controls.Add(this.pbx_1);
            this.Controls.Add(this.pbx_2);
            this.Controls.Add(this.pbx_4);
            this.Controls.Add(this.pbx_5);
            this.Controls.Add(this.pbx_3);
            this.Controls.Add(this.pbx_6);
            this.Controls.Add(this.pbx_lvl6);
            this.Controls.Add(this.pbx_zlvl2);
            this.Controls.Add(this.pbx_lvl3);
            this.Controls.Add(this.pbx_zlvl1);
            this.Controls.Add(this.pbx_lvl2);
            this.Controls.Add(this.pbx_lvl1);
            this.Controls.Add(this.pbx_lvl5);
            this.Controls.Add(this.pbx_lvl4);
            this.Controls.Add(this.rbtn_ich);
            this.Controls.Add(this.rbtn_alle);
            this.Controls.Add(this.btn_daten);
            this.Controls.Add(this.btn_account);
            this.Controls.Add(this.cbx_level);
            this.Controls.Add(this.pbx_grau3);
            this.Controls.Add(this.pbx_grau2);
            this.Controls.Add(this.pbx_grau1);
            this.Controls.Add(this.pbx_rot1);
            this.Controls.Add(this.pbx_rot2);
            this.Controls.Add(this.pbx_rot3);
            this.Controls.Add(this.btn_ausloggen);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.pbx_avatar);
            this.Controls.Add(this.lbx_board);
            this.Controls.Add(this.btn_signup);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.btn_loschen);
            this.Controls.Add(this.tbx_passwort);
            this.Controls.Add(this.tbx_benutzer);
            this.Controls.Add(this.lbl_passwort);
            this.Controls.Add(this.lbl_benutzer);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frm_main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.frm_login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_zlvl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_zlvl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_lvl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_grau1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_rot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_avatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_benutzer;
        private System.Windows.Forms.Label lbl_passwort;
        private System.Windows.Forms.TextBox tbx_benutzer;
        private System.Windows.Forms.TextBox tbx_passwort;
        private System.Windows.Forms.Button btn_loschen;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Button btn_signup;
        private System.Windows.Forms.ListBox lbx_board;
        private System.Windows.Forms.PictureBox pbx_avatar;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Button btn_ausloggen;
        private System.Windows.Forms.PictureBox pbx_rot3;
        private System.Windows.Forms.PictureBox pbx_rot2;
        private System.Windows.Forms.PictureBox pbx_rot1;
        private System.Windows.Forms.PictureBox pbx_grau1;
        private System.Windows.Forms.PictureBox pbx_grau2;
        private System.Windows.Forms.PictureBox pbx_grau3;
        private System.Windows.Forms.ComboBox cbx_level;
        private System.Windows.Forms.Button btn_account;
        private System.Windows.Forms.Button btn_daten;
        private System.Windows.Forms.RadioButton rbtn_alle;
        private System.Windows.Forms.RadioButton rbtn_ich;
        private System.Windows.Forms.Button btn_zusatz1;
        private System.Windows.Forms.Button btn_zusatzlvl2;
        private System.Windows.Forms.PictureBox pbx_3;
        private System.Windows.Forms.PictureBox pbx_6;
        private System.Windows.Forms.PictureBox pbx_1;
        private System.Windows.Forms.PictureBox pbx_2;
        private System.Windows.Forms.PictureBox pbx_7;
        private System.Windows.Forms.PictureBox pbx_4;
        private System.Windows.Forms.PictureBox pbx_5;
        private System.Windows.Forms.PictureBox pbx_lvl4;
        private System.Windows.Forms.PictureBox pbx_lvl5;
        private System.Windows.Forms.PictureBox pbx_lvl1;
        private System.Windows.Forms.PictureBox pbx_lvl2;
        private System.Windows.Forms.PictureBox pbx_zlvl1;
        private System.Windows.Forms.PictureBox pbx_lvl3;
        private System.Windows.Forms.PictureBox pbx_zlvl2;
        private System.Windows.Forms.PictureBox pbx_lvl6;
    }
}