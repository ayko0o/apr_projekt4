﻿namespace semesterprojekt_1
{
    partial class frm_zusatzlvl2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_frage = new System.Windows.Forms.Label();
            this.lbl_hs = new System.Windows.Forms.Label();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.lbl_falsch1 = new System.Windows.Forms.Label();
            this.lbl_falsch2 = new System.Windows.Forms.Label();
            this.lbl_falsch3 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lbl_fragee = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_frage
            // 
            this.lbl_frage.Font = new System.Drawing.Font("Copperplate Gothic Bold", 15F);
            this.lbl_frage.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lbl_frage.Location = new System.Drawing.Point(98, 148);
            this.lbl_frage.Name = "lbl_frage";
            this.lbl_frage.Size = new System.Drawing.Size(533, 37);
            this.lbl_frage.TabIndex = 0;
            this.lbl_frage.Text = "Welches dieser Länder ist kein Binnenland?";
            this.lbl_frage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_hs
            // 
            this.lbl_hs.AutoSize = true;
            this.lbl_hs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_hs.Location = new System.Drawing.Point(12, 90);
            this.lbl_hs.Name = "lbl_hs";
            this.lbl_hs.Size = new System.Drawing.Size(80, 24);
            this.lbl_hs.TabIndex = 1;
            this.lbl_hs.Text = "Punkte:";
            // 
            // btn_1
            // 
            this.btn_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
            this.btn_1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold);
            this.btn_1.Location = new System.Drawing.Point(198, 280);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(258, 57);
            this.btn_1.TabIndex = 2;
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btn_2
            // 
            this.btn_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
            this.btn_2.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold);
            this.btn_2.Location = new System.Drawing.Point(198, 360);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(258, 57);
            this.btn_2.TabIndex = 3;
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_3
            // 
            this.btn_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
            this.btn_3.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold);
            this.btn_3.Location = new System.Drawing.Point(198, 435);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(258, 57);
            this.btn_3.TabIndex = 4;
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // lbl_falsch1
            // 
            this.lbl_falsch1.AutoSize = true;
            this.lbl_falsch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.lbl_falsch1.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_falsch1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch1.Location = new System.Drawing.Point(477, 297);
            this.lbl_falsch1.Name = "lbl_falsch1";
            this.lbl_falsch1.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch1.TabIndex = 5;
            this.lbl_falsch1.Text = "Falsch";
            this.lbl_falsch1.Visible = false;
            // 
            // lbl_falsch2
            // 
            this.lbl_falsch2.AutoSize = true;
            this.lbl_falsch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.lbl_falsch2.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_falsch2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch2.Location = new System.Drawing.Point(477, 379);
            this.lbl_falsch2.Name = "lbl_falsch2";
            this.lbl_falsch2.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch2.TabIndex = 6;
            this.lbl_falsch2.Text = "Falsch";
            this.lbl_falsch2.Visible = false;
            // 
            // lbl_falsch3
            // 
            this.lbl_falsch3.AutoSize = true;
            this.lbl_falsch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.lbl_falsch3.Font = new System.Drawing.Font("MV Boli", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_falsch3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbl_falsch3.Location = new System.Drawing.Point(477, 454);
            this.lbl_falsch3.Name = "lbl_falsch3";
            this.lbl_falsch3.Size = new System.Drawing.Size(54, 20);
            this.lbl_falsch3.TabIndex = 7;
            this.lbl_falsch3.Text = "Falsch";
            this.lbl_falsch3.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::semesterprojekt_1.Properties.Resources.fishy;
            this.pictureBox6.Location = new System.Drawing.Point(606, 507);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(72, 33);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 13;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::semesterprojekt_1.Properties.Resources.fishy;
            this.pictureBox5.Location = new System.Drawing.Point(652, 507);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(122, 56);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::semesterprojekt_1.Properties.Resources.fishy;
            this.pictureBox4.Location = new System.Drawing.Point(587, 391);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(204, 110);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.pictureBox3.Location = new System.Drawing.Point(-8, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(771, 58);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.pictureBox1.Location = new System.Drawing.Point(152, 253);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(413, 272);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.pictureBox2.Location = new System.Drawing.Point(51, 132);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(627, 72);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::semesterprojekt_1.Properties.Resources.froggy;
            this.pictureBox8.Location = new System.Drawing.Point(629, 104);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(49, 37);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 15;
            this.pictureBox8.TabStop = false;
            // 
            // lbl_fragee
            // 
            this.lbl_fragee.AutoSize = true;
            this.lbl_fragee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(230)))));
            this.lbl_fragee.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_fragee.Location = new System.Drawing.Point(274, 31);
            this.lbl_fragee.Name = "lbl_fragee";
            this.lbl_fragee.Size = new System.Drawing.Size(139, 24);
            this.lbl_fragee.TabIndex = 16;
            this.lbl_fragee.Text = "Frage 1 von 5";
            // 
            // frm_zusatzlvl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(728, 593);
            this.Controls.Add(this.lbl_fragee);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lbl_falsch3);
            this.Controls.Add(this.lbl_falsch2);
            this.Controls.Add(this.lbl_falsch1);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.lbl_hs);
            this.Controls.Add(this.lbl_frage);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox8);
            this.Name = "frm_zusatzlvl2";
            this.Text = "frm_zusatzlvl2";
            this.Load += new System.EventHandler(this.frm_zusatzlvl2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_frage;
        private System.Windows.Forms.Label lbl_hs;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Label lbl_falsch1;
        private System.Windows.Forms.Label lbl_falsch2;
        private System.Windows.Forms.Label lbl_falsch3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lbl_fragee;
    }
}